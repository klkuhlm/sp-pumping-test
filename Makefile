#
# Copyright (c) 2011 Kristopher L. Kuhlman (klkuhlm at sandia dot gov)
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
# THE SOFTWARE.
#
# Malama, B., A. Revil, and K.L. Kuhlman, 2009. A semi-analytical solution
#  for transient streaming potentials associated with confined aquifer
#  pumping tests, Geophysical Journal International, 176(3), 1007–1016.
# http://dx.doi.org/10.1111/j.1365-246X.2008.04014.x
#
# Malama, B., K.L. Kuhlman, and A. Revil, 2009. Theory of transient
#  streaming potentials associated with axial-symmetric flow in unconfined
#  aquifers, Geophysical Journal International, 179(2), 990–1003.
# http://dx.doi.org/10.1111/j.1365-246X.2009.04336.x
#

###### Gfortran >= 4.6 ############################
#
PERF=-Ofast -march=native #-mtune=core2 -static
DEBUG=-O0 -g -fbounds-check -frange-check -Wall -Wextra
DEBUG+=-fcheck=all 
DEFAULTS=-fdefault-real-8 -fdefault-integer-8
PERF+=$(DEFAULTS)
DEBUG+=$(DEFAULTS)
F90=gfortran-4.7
PERFLDFLAGS=-static
CPP=-cpp
##################################################


KKFILES = laplace_fhankel_solutions.o
OBJS = utility.o invlap.o $(KKFILES)

MAIN = driver-finite.o

OPTOBJS = $(patsubst %.o,%.opt.o,$(OBJS) $(MAIN))
DEBUGOBJS = $(patsubst %.o,%.debug.o,$(OBJS) $(MAIN))

F90SRC=$(patsubst %.o,%.f90,$(OBJS) $(MAIN))

OUT = sp_driver
DEBUGOUT = debug_sp_driver

LD = $(F90)

####### default optimized (no debugging) target ##########################
driver: $(OPTOBJS)
	$(LD)  $(PERFLDFLAGS) -o $(OUT) $(OPTOBJS)

####### compiler debugging ###
##(no optimization, checks for out-of-bounds arrays and gives more warninngs, but still runs to completion)
debug_driver: $(DEBUGOBJS)
	$(LD) -o $(DEBUGOUT) $(DEBUGOBJS)

####### rule for making optimized object files ############
%.opt.o: %.f90
	$(F90) -c -cpp $(INTEL) $(PERF) -o $@ $<

####### rule for making debugging object files ############
%.debug.o: %.f90
	$(F90) -c -cpp $(INTEL) $(DEBUG) -o $@ $<

utility.opt.o constants.mod shared_data.mod invlap_fcns.mod: utility.f90
laplace_fhankel_solutions.opt.o three_layer_finite.mod: \
 laplace_fhankel_solutions.f90 shared_data.mod constants.mod \
 constants.mod
driver-finite.opt.o: driver-finite.f90 shared_data.mod constants.mod \
 invlap_fcns.mod three_layer_finite.mod constants.mod constants.mod

utility.debug.o constants.mod shared_data.mod invlap_fcns.mod: utility.f90
laplace_fhankel_solutions.debug.o three_layer_finite.mod: \
 laplace_fhankel_solutions.f90 shared_data.mod constants.mod \
 constants.mod
driver-finite.debug.o: driver-finite.f90 shared_data.mod constants.mod \
 invlap_fcns.mod three_layer_finite.mod constants.mod constants.mod


###### clean up #################################
clean:
	rm -f *.o *.mod $(OUT) $(DEBUGOUT) $(MATOUT)

# create list of dependencies using compiler
dep:
	gfortran-4.7 -M -cpp $(F90SRC) > dep.out && echo -e "\n\t***\ndependencies output to dep.out\n\t***\n"
