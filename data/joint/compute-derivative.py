import numpy as np
import matplotlib.pyplot as plt
from scipy.interpolate import UnivariateSpline

test = '07082011'

t0 = 2940.0 # ~pumping began (seconds)
t1 = 14780.0 # ~pumping ended (seconds)

PSI2MH2O = 0.70377 # or 2.3089 feet

sall = np.loadtxt('labSP%sdrawdown.txt' % test)
sth = np.loadtxt('head.time')
ss = np.loadtxt('head.samp')
#sm = np.loadtxt('head.out',skiprows=16)

logt = np.log(sth)

# fit spline to data
spline = UnivariateSpline(logt,ss,s=2.5E-6,k=5)

splinef = np.empty_like(sth)
splinedf = np.empty_like(sth)

for j,tval in enumerate(logt):
    derivs = spline.derivatives(tval)
    splinef[j],splinedf[j] = derivs[0:2]

np.savetxt('head.deriv',splinedf,fmt='%.3e')

plt.figure(1)
plt.semilogx(sall[:,0],sall[:,3]*PSI2MH2O,'r.')
plt.semilogx(sth,splinef,'k-')
plt.semilogx(sth,splinedf,'k--')
#plt.semilogx(sm[:,0],sm[:,4],'b-')
#plt.semilogx(sm[:,0],sm[:,5],'b--')
plt.savefig('head-with-derivs.png')
plt.close(1)

E023 = np.loadtxt('SP%sE023corr.txt' % test)
E023t = np.loadtxt('E023.time')
E023phi = np.loadtxt('E023.meas')
#E023m = np.loadtxt('E023.out',skiprows=16)

logt = np.log(E023t)
spline = UnivariateSpline(logt,E023phi,s=1.0E-8,k=5)

splinef = np.empty_like(logt)
splinedf = np.empty_like(logt)

for j,tval in enumerate(logt):
    derivs = spline.derivatives(tval)
    splinef[j],splinedf[j] = derivs[0:2]

np.savetxt('E023.deriv',splinedf,fmt='%.3e')

plt.figure(1)
plt.semilogx(E023t,E023phi,'r.')
plt.semilogx(E023t,splinef,'k-')
plt.semilogx(E023t,splinedf,'k--')
#plt.semilogx(E023m[:,0],E023m[:,1],'b-')
#plt.semilogx(E023m[:,0],E023m[:,2],'b--')
plt.savefig('E023-with-derivs.png')
plt.close(1)

E024 = np.loadtxt('SP%sE024corr.txt' % test)
E024t = np.loadtxt('E024.time')
E024phi = np.loadtxt('E024.meas')
#E024m = np.loadtxt('E024.out',skiprows=16)

logt = np.log(E024t)
spline = UnivariateSpline(logt,E024phi,s=1.0E-8,k=5)

splinef = np.empty_like(logt)
splinedf = np.empty_like(logt)

for j,tval in enumerate(logt):
    derivs = spline.derivatives(tval)
    splinef[j],splinedf[j] = derivs[0:2]

np.savetxt('E024.deriv',splinedf,fmt='%.3e')

plt.figure(1)
plt.semilogx(E024t,E024phi,'r.')
plt.semilogx(E024t,splinef,'k-')
plt.semilogx(E024t,splinedf,'k--')
#plt.semilogx(E024m[:,0],E024m[:,1],'b-')
#plt.semilogx(E024m[:,0],E024m[:,2],'b--')
plt.savefig('E024-with-derivs.png')
plt.close(1)

E025 = np.loadtxt('SP%sE025corr.txt' % test)
E025t = np.loadtxt('E025.time')
E025phi = np.loadtxt('E025.meas')
#E025m = np.loadtxt('E025.out',skiprows=16)

logt = np.log(E025t)
spline = UnivariateSpline(logt,E025phi,s=1.0E-8,k=5)

splinef = np.empty_like(logt)
splinedf = np.empty_like(logt)

for j,tval in enumerate(logt):
    derivs = spline.derivatives(tval)
    splinef[j],splinedf[j] = derivs[0:2]

np.savetxt('E025.deriv',splinedf,fmt='%.3e')

plt.figure(1)
plt.semilogx(E025t,E025phi,'r.')
plt.semilogx(E025t,splinef,'k-')
plt.semilogx(E025t,splinedf,'k--')
#plt.semilogx(E025m[:,0],E025m[:,1],'b-')
#plt.semilogx(E025m[:,0],E025m[:,2],'b--')
plt.savefig('E025-with-derivs.png')
plt.close(1)
