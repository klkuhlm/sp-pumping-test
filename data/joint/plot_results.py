import numpy as np
import matplotlib.pyplot as plt

test = '07082011'

mv = 1000.0 # change volts to millivolts

t0 = 2940.0 # ~pumping began (seconds)
t1 = 14780.0 # ~pumping ended (seconds)

PSI2MH2O = 0.70377 # or 2.3089 feet

# all data
sall = np.loadtxt('labSP%sdrawdown.txt' % test)
sth = np.loadtxt('head.time')
ss = np.loadtxt('head.samp')
sm = np.loadtxt('head.out',skiprows=16)
ds = np.loadtxt('head.deriv')

E023 = np.loadtxt('SP%sE023corr.txt' % test)
E023t = np.loadtxt('E023.time')
E023phi = np.loadtxt('E023.meas')
E023m = np.loadtxt('E023.out',skiprows=16)
dE023 = np.loadtxt('E023.deriv')

E024 = np.loadtxt('SP%sE024corr.txt' % test)
E024t = np.loadtxt('E024.time')
E024phi = np.loadtxt('E024.meas')
E024m = np.loadtxt('E024.out',skiprows=16)
dE024 = np.loadtxt('E024.deriv')

E025 = np.loadtxt('SP%sE025corr.txt' % test)
E025t = np.loadtxt('E025.time')
E025phi = np.loadtxt('E025.meas')
E025m = np.loadtxt('E025.out',skiprows=16)
dE025 = np.loadtxt('E025.deriv')

fig = plt.figure(1,figsize=(14,14))
ax1 = fig.add_subplot(221)
#ax1.semilogx(sth,ss,'yo')
ax1.semilogx(sall[:,0],sall[:,3]*PSI2MH2O,'ko',markersize=0.75) # observed drawdown
ax1.semilogx(sm[:,0],sm[:,4],'k-') # modeled drawdown
ax1.semilogx(sth,ds,'k--') # splined "observed" log(t) derivative
ax1.semilogx(sm[:,0],sm[:,5],'k:') # modeled log(t) derivative
ax1.set_xlim([sall[:,0].min(),sall[:,0].max()])
ax1.set_xlabel('time [s]')
ax1.set_ylabel('$s$ [m] | $\\partial s/\\partial(\\log(t)$')
ax1.set_title('24" piezometer')

ax2 = fig.add_subplot(222)
m = np.logical_and(E023[:,0] > t0, E023[:,0] < t1)
E023mt = E023[m,0]-t0
#ax2.semilogx(E023t,E023phi*mv,'ro')
ax2.semilogx(E023mt,E023[m,1]*mv,'ro',markersize=0.75)
ax2.semilogx(E023m[:,0],E023m[:,3]*mv,'r-')
ax2.semilogx(E023t,dE023*mv,'r--')
ax2.semilogx(E023m[:,0],E023m[:,2]*mv,'r:')
ax2.set_xlim([E023mt.min(),E023mt.max()])
ax2.set_xlabel('time [s]')
ax2.set_ylabel('$\\Delta \\phi$ (mV) | $\\partial \\phi/\\partial(\\log(t)$')
ax2.set_title('E023')

ax3 = fig.add_subplot(223)
m = np.logical_and(E024[:,0] > t0, E024[:,0] < t1)
E024mt = E024[m,0]-t0
#ax3.semilogx(E024t,E024phi*mv,'go')
ax3.semilogx(E024mt,E024[m,1]*mv,'go',markersize=0.75)
ax3.semilogx(E024m[:,0],E024m[:,3]*mv,'g-')
ax3.semilogx(E024t,dE024*mv,'g--')
ax3.semilogx(E024m[:,0],E024m[:,2]*mv,'g:')
ax3.set_xlim([E024mt.min(),E024mt.max()])
ax3.set_xlabel('time [s]')
ax3.set_ylabel('$\\Delta \\phi$ (mV) | $\\partial \\phi/\\partial(\\log(t)$')
ax3.set_title('E024')

ax4 = fig.add_subplot(224)
m = np.logical_and(E025[:,0] > t0, E025[:,0] < t1)
E025mt = E025[m,0]-t0
#ax4.semilogx(E025t,E025phi*mv,'bo')
ax4.semilogx(E025[m,0]-t0,E025[m,1]*mv,'bo',markersize=0.75,label='meas SP')
ax4.semilogx(E025m[:,0],E025m[:,3]*mv,'b-',label='model SP')
ax4.semilogx(E025t,dE025*mv,'b--',label='meas dSP')
ax4.semilogx(E025m[:,0],E025m[:,2]*mv,'b:',label='mod dSP')
ax4.set_xlim([E025mt.min(),E025mt.max()])
ax4.set_xlabel('time [s]')
ax4.set_ylabel('$\\Delta \\phi$ (mV) | $\\partial \\phi/\\partial(\\log(t)$')
ax4.legend(loc=0)
ax4.set_title('E025')

plt.subplots_adjust(wspace=0.25)
plt.suptitle('PEST results %s E023-E025' % test)
plt.savefig('pest_results.png')
