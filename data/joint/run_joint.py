import numpy as np
from glob import glob

deriv = True

PSI2MH2O = 0.70377 # or 2.3089 feet
IN2M = 0.0254 # meters in an inch

t0 = 2940.0 # ~pumping began (seconds)
t1 = 14780.0 # ~pumping ended (seconds)

alldata = {}
derivdata = {}

with open('electrodes-configuration-test.csv','r') as fh:
    lines = fh.readlines()[1:]

elec = {}
for line in lines:
    f = line.strip().split(',')

    # key is electrode (capital E)
    # fields are: electrode, channel, r (inches), depth (inches), angle (degrees CCW from north)
    elec[f[0].upper()] = {'r':float(f[2])*IN2M,
                          'depth':float(f[3])*IN2M}

Q = 0.2/1000.0/60.0 # <- m^3/sec from 0.2 L/min

#test = '08052011'
test = '07082011'

# ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

headdatafile = 'labSP'+test+'drawdown.txt'

# all drawdown data are in one file, three columns of data
d = np.loadtxt(headdatafile)

skip = 4
m1 = d[:,0]<100
m2 = np.logical_and(d[:,0]>100,d[:,0]<1000)
m3 = d[:,0] > 1000
td = np.concatenate((d[m1,0],d[m2,0][::skip],d[m3,0][::skip*6]),axis=0)
nhead = td.shape[0]



  # 24 inches (least partial-penetration effects)
s = np.concatenate((d[m1,3],d[m2,3][::skip],d[m3,3][::skip*6]),axis=0)*PSI2MH2O

alldata['head'] = s

if deriv:
    derivdata['head'] = np.loadtxt('head.deriv')

timefn = 'head.time'

# write log-spaced times computed above
np.savetxt(timefn,td,fmt='%.1f')
np.savetxt('head.samp',s,fmt='%4e')

# pest template file for head
ptffn = 'head.ptf'
ptfh = open(ptffn,'w')

ptfh.write("""ptf &
0  F  8.5D-1  T  T  F     :: debug, dimensionless?, rtank, acceleration?, flow?, sp?
&     Q        &  0.0D+0  1.0D+299   :: pumping rate, pump andrecovery time
2.0D-1  4.0D-1  8.0D-2         :: b1, b2, b3
&     Kr       & &   kappa      & &    Ss        & &    Sy        & &   beta       & :: aquifer Kr, kappa, Ss, Sy, and beta
5.0D-2 5.0D-2  5.0D-2  :: sigma for layers 1,2,3
-1.0D-4     :: gamma*ell for aquifer
6.096D-1  3.0D-1                :: Obs loc: r, depth
1.0D-7  1.0D-8  25         :: deHoog parameters
50                         :: finite hankel sum length
F  -6  3  %i  %s   :: calc t?, min log_10(t), max log_10(t), numt, t filename
0.0D+0  0.0D+0  1.0D+0             :: slope, intercept of trend, sign of solution
head.out
""" % (nhead,timefn))
ptfh.close()

# pest instruction file for head
piffn = 'head.ins'
pifh = open(piffn,'w')

pifh.write('pif *\n*#--------------------------------------*\n')
for k in range(nhead):
    # head is column 5
    pifh.write('l1 [s-%4.4i]89:109\n' % (k+1))
pifh.close()

if deriv:
    piffn = 'head-deriv.ins'
    pifh = open(piffn,'w')

    pifh.write('pif *\n*#--------------------------------------*\n')
    for k in range(nhead):
        # log-derivative of head is column 6
        pifh.write('l1 [ds-%4.4i]111:131\n' % (k+1))
    pifh.close()


# ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

for en in [23,24,25]:

    e = 'E%3.3i' % en
    spdatafile = 'SP%s%scorr.txt' % (test,e)

    # two header lines
    # columns are time (seconds) and SP (volts)
    d = np.loadtxt(spdatafile,skiprows=2)

    tmask = np.logical_and(d[:,0] < t1,d[:,0] > t0)
    ttest = d[tmask,0] - t0 # zero is beginning of test
    phitest = d[tmask,1]

    skip = 3
    m1 = ttest<100
    m2 = np.logical_and(ttest>100,ttest<1000)
    m3 = ttest > 1000
    tsp = np.concatenate((ttest[m1],ttest[m2][::skip],ttest[m3][::skip*6]),axis=0)
    phi = np.concatenate((phitest[m1],phitest[m2][::skip],phitest[m3][::skip*6]),axis=0)

    alldata[e] = phi

    if deriv:
        derivdata[e] = np.loadtxt(e+'.deriv')

    sign = 1.0
    print e

    nsp = tsp.shape[0]

    # pest template file for SP
    ptffn = e+'.ptf'
    ptfh = open(ptffn,'w')

    timefn = e+'.time'

    # write log-spaced times computed above
    np.savetxt(timefn,tsp,fmt='%.1f')
    np.savetxt(e+'.meas',phi,fmt='%.5e')

    ptfh.write("""ptf &
0  F  8.5D-1  T  F  T                :: debug, dimensionless?, rtank, acceleration?, flow?, SP?
&     Q        &  0.0D+0  1.0D+299      :: pumping rate, pump and recovery time
2.0D-1  4.0D-1  8.0D-2         :: b1, b2, b3
&     Kr       & &   kappa      & &    Ss        & &    Sy        & &   beta       & :: aquifer Kr, kappa, Ss, Sy, and beta
&  sigma1      & &  sigma2      & &  sigma3      &  :: sigma for layers 1,2,3
&    gamel     &     :: gamma*ell for aquifer
%.7f  %.7f                 :: Obs loc: r, depth
1.0D-7  1.0D-8  25         :: deHoog parameters
50                         :: finite hankel sum length
F  -6  3  %i  %s   :: calc t?, min log_10(t), max log_10(t), numt, t filename
0.0D+0  &    shift%s    &  1.0D+0             :: slope, intercept of trend, sign of solution
%s.out
""" % (elec[e]['r'],elec[e]['depth'],nsp,timefn,e,e))
    ptfh.close()

    # pest instruction file
    piffn = e+'.ins'
    pifh = open(piffn,'w')

    pifh.write('pif *\n*#--------------------------------------*\n')
    for j in range(nsp):
        # unadjusted phi is second column
        #pifh.write('l1 [phi%s-%4.4i]23:43\n' % (e,j+1))
        # adjusted phi is  fourth column
        pifh.write('l1 [phi%s-%4.4i]67:87\n' % (e,j+1))
    pifh.close()

    if deriv:
        # pest instruction file
        piffn = e+'-deriv.ins'
        pifh = open(piffn,'w')

        pifh.write('pif *\n*#--------------------------------------*\n')
        for j in range(nsp):
            # log-derivative of phi is third column
            pifh.write('l1 [dphi%s-%4.4i]45:65\n' % (e,j+1))
        pifh.close()


# ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

# pest control file
pcfh = open('joint_head_sp_e23-25.pst','w')

ndtotal = sum([len(alldata[k]) for k in alldata.keys()])
print 'length of all data:',ndtotal

if deriv:
    derivtotal = sum([len(derivdata[k]) for k in derivdata.keys()])
    print 'length of all deriv data:',derivtotal

# compute weights to help balance different data types
weights = {}
for k in alldata.keys():
    weights[k] = len(alldata[k])/float(ndtotal)

hw = weights['head']

# normalize so head is 1
for k in alldata.keys():
    weights[k] = weights[k]/hw

# heads are dominating calibration (reduce weight)
weights['head'] = 0.25

# factor for derivative compared to function
derivfactor = 2.0

if deriv:
    totalobs = ndtotal + derivtotal
    ngroups = 4
    ninsfiles = 8
else:
    totalobs = ndtotal
    ngroups = 2
    ninsfiles = 4

pcfh.write("""pcf
* control data
restart estimation
13  %i  1  0  %i
4  %i  double point  1  0  0
5  2  0.3  0.03  10  0
3  3  0.001
0.1
30  0.01  3  3  0.01  3
1  1  1
* parameter groups
sppar  relative  0.01  1.00E-20  switch  2.0  parabolic
* parameter data
Q     fixed   factor     %.7e      1.0E-8   1.0E+8  sppar  1  0  1
Kr     log   factor    6.4E-5   1.0E-8   1.0E+8  sppar  1  0  1
kappa  log   factor    2.0E+0      1.0E-8   1.0E+8  sppar  1  0  1
Ss     log   factor    3.0E-5   1.0E-10  1.0     sppar  1  0  1
Sy     log   factor    0.14    1.0E-10  0.5     sppar  1  0  1
beta   fixed  relative  0.0      0.0      10.0    sppar  1  0  1
sigma1 log   factor    4.20E-6  1.0E-12  1.0E+4  sppar  1  0  1
sigma2 log   factor    4.20E-3  1.0E-12  1.0E+4  sppar  1  0  1
sigma3 log   factor    2.0E+0  1.0E-12  1.0E+4  sppar  1  0  1
gamel  log   factor    2.5E-2  1.0E-12  1.0E+12 sppar  -1  0  1
shifte023  fixed relative  0.0  -1.0  1.0  sppar 1 0 1
shifte024  fixed relative  0.0  -1.0  1.0  sppar 1 0 1
shifte025  fixed relative  0.0  -1.0  1.0  sppar 1 0 1
* observation groups
headobs
spobs
""" % (totalobs,ngroups,ninsfiles,Q))

# head-heavy solution:
# Parameter        Estimated         95% percent confidence limits
#                  value             lower limit       upper limit
#  kr             6.399653E-05       5.583211E-05      7.335485E-05
#  kappa          4.220733E-02       2.578420E-02      6.909109E-02
#  ss             3.501299E-02       2.445179E-02      5.013577E-02
#  sy             0.347556           0.235944          0.511966    
#  sigma1         5.435724E-07       5.435724-307      5.435724+293
#  sigma2         0.304240           3.042401-301      3.042401+299
#  sigma3          2.04957           2.049566-300      1.000000+300
#  gamel          2.427153E-02       2.427153-302      2.427153+298

# sp-heavy solution


if deriv:
    pcfh.write('headderiv\nspderiv\n* observation data\n')
else:
    pcfh.write('* observation data\n')

# head data
for j in range(nhead):
    pcfh.write('s-%4.4i  %.4e  %.4f  headobs\n' %
               (j+1,alldata['head'][j],weights['head']))

if deriv:
    for j in range(nhead):
        pcfh.write('ds-%4.4i  %.4e  %.4f  headderiv\n' %
                   (j+1,derivdata['head'][j],weights['head']*derivfactor))

# sp data for three electrodes
for e in ['E023','E024','E025']:
    for j in range(len(alldata[e])):
        pcfh.write('phi%s-%4.4i  %.4e  %.4f  spobs\n' %
                   (e,j+1,alldata[e][j],weights[e]))

if deriv:
    for e in ['E023','E024','E025']:
        for j in range(len(alldata[e])):
            pcfh.write('dphi%s-%4.4i  %.4e  %.4f  spderiv\n' %
                       (e,j+1,derivdata[e][j],weights[e]*derivfactor))

if deriv:
    pcfh.write("""* model command line
./run_joint_driver
* model input/output
head.ptf head.dat
E023.ptf E023.dat
E024.ptf E024.dat
E025.ptf E025.dat
head.ins head.out
head-deriv.ins head.out
E023.ins E023.out
E023-deriv.ins E023.out
E024.ins E024.out
E024-deriv.ins E024.out
E025.ins E025.out
E025-deriv.ins E025.out
* prior information
""")
else:
    pcfh.write("""* model command line
./run_joint_driver
* model input/output
head.ptf head.dat
E023.ptf E023.dat
E024.ptf E024.dat
E025.ptf E025.dat
head.ins head.out
E023.ins E023.out
E024.ins E024.out
E025.ins E025.out
* prior information
""")
pcfh.close()
