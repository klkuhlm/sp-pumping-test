% assume the periodic noise to be removed consists of the product of two components
% 1) a unit-amplitude Walsh function (0,1) of given frequency and phase
% 2) a sin/cos function of known frequency, but unknown amplitude and phase
%
% the Walsh function is used to turn the sine on and off in a periodic manner.

clear;
clf;

% seconds defining post test period (do least squares from here to end)
posttest = 60*60*14 % 14 hours?

% phase shift of Walsh function (seconds)
% Walsh function starts a half-period of ones at phase=0
Wphase = 20*60*60 % time of beginning of trane of periodic disturbances
% period of Walsh function (seconds)
Wperiod = 24*60*60 % ~one day in seconds

% high-frequency function
hperiod = 3*60*60; % ~three hours in seconds

testname = "SP08052011";

Won = 2/3.0; % fraction of a period that Walsh funciton is "on"

for j=1:1
    % read in the data (seconds,volts)
    elec = sprintf("E%03d",j)
    data = load([testname,elec,".txt"]);

    % all data
    nn = size(data,1);

    % just later data for fitting
    mask = data(:,1) > posttest;
    t = data(mask,1);
    v = data(mask,2);

    % number of datapoints to perform least-squares fitting on
    n = size(t,1);

    % assume times are monotonically increasing
    t0 = t(1);
    t1 = t(end);
    lengtht = t1-t0
    scale = 2.0*pi/lengtht

    % shift phase to same zero as LS fitting dataset
    Wphase = (Wphase-t0)

    % scale the high-frequency period and convert to frequency
    hfreq = 1/(hperiod)

    % 1:shift, 2:linear trend, 3:complex amplitude of exponential
    A = zeros(n,3);

    % shift is a constant
    A(:,1) = 1;

    % linear trend is proportional to time
    A(:,2) = t(:,1);

    % create a sawtooth function with period Wperiod and unit amplitude
    Warg = mod((t(:) - Wphase)./Wperiod,1);

    % Walsh function is zero second half of period
    % and one the first half. Convert sawtooth to positive square wave
    W = zeros(n,1);
    W(Warg < Won) = 1.0;

    jpi = -sqrt(-1.0)*scale;

    A(:,3) = W(:).*exp(jpi*t(:)*hfreq);

    % compute least-squares solution
    x = A\v

    subplot(121);
    plot(t,(v-mean(v))./(max(v)-min(v)));
    hold on;
    plot(t,W);

    subplot(122);
    % original data (red line)
    plot(data(:,1),data(:,2),'r-');
    hold on;

    % apply trends computed above (x coefficients) to entire line (not just post-test)
    A = ones(nn,3);
    A(:,2) = data(:,1);
    scale = 2.0*pi/(data(end,1) - data(1,1));
    tsc = scale.*(data(:,1) - data(1,1));
    Warg = mod((tsc(:) - Wphase)./Wperiod,1);
    W = zeros(nn,1);
    W(Warg < Won) = 1.0;
    A(:,3) = W(:).*exp(-jpi*tsc(:)*hfreq);

    res = data(:,2) - A*x;

    plot(data(:,1),res,'g-');
    xlabel('time (s)');
    ylabel('SP (V)');
    yv = ylim();
    text(0.7,0.9,['const =',num2str(abs(x(1)))],'Units','normalized');
    text(0.7,0.85,['slope =',num2str(abs(x(2)))],'Units','normalized');
    text(0.7,0.8,['amplitude =',num2str(abs(x(3)))],'Units','normalized');
    text(0.7,0.75,['phase =',num2str(angle(x(3))/pi),'*pi'],'Units','normalized');
    ylim(yv);
    title([testname,' ',elec]);
    print('-depsc2',[testname,elec,'.eps']);
end
