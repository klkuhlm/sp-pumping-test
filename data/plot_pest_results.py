import numpy as np
import matplotlib
matplotlib.use('Agg')
import matplotlib.pyplot as plt
from glob import glob

prefix = 'SP07082011'

measfn = glob(prefix+'*.txt')[0]

meas = np.loadtxt(measfn,skiprows=2)
mod =  np.loadtxt(glob(prefix+'*.out')[0],skiprows=16,usecols=(0,3))

with open(glob(prefix+'*.par')[0]) as fh: 
    lines = fh.readlines()[1:]

latexnames = ['$K_r$','$\\kappa$','$S_s$','$S_y$','$\\beta$',
              '$\\sigma_1$','$\\sigma_2$','$\\sigma_3$','$\\gamma \\ell$',
              'growth','shift','$t_0$','$t_1$']

pars = []
for j,line in enumerate(lines):
    f = line.split()
    # read in best-fit parameters
    pars.append((f[0],float(f[1]),latexnames[j]))

fig = plt.figure(figsize=(11,9))
ax = fig.add_subplot(121)
ax.semilogx(meas[:,0],meas[:,1],'.',label='measured')
ax.semilogx(mod[:,0],mod[:,1],'-',label='modeled')
ax.set_xlabel('t (sec)')
ax.set_ylabel('SP (volts)')
ax.set_title(prefix + ' ' + measfn.rstrip('.txt').lstrip(prefix))
plt.legend(loc=0)
ax = fig.add_subplot(122)
ax.plot()
plt.annotate('\n'.join(['%s=%.3e' % (c,b) for (a,b,c) in pars]),(0.25,0.25))
plt.savefig(measfn.replace('.txt','.eps'))


