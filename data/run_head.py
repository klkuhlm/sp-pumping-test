import numpy as np
from glob import glob

PSI2MH2O = 0.70377 # or 2.3089 feet

IN2M = 0.0254 # meters in an inch

Q = 0.2/1000.0/60.0 # <- m^3/sec from 0.2 L/min

#test = '08052011'
test = '07082011'

datafile = 'labSP'+test+'drawdown.txt'

# all drawdown data are in one file, three columns of data
d = np.loadtxt(datafile)

skip = 7
t = d[::skip,0]
nd = t.shape[0]
s = np.zeros((nd,3))
s[:,0] = d[::skip,1]*PSI2MH2O  # 8 inches
s[:,1] = d[::skip,2]*PSI2MH2O  # 16 inches
s[:,2] = d[::skip,3]*PSI2MH2O  # 24 inches

weights = [0.25,0.5,1.0]

timefn = datafile.replace('.txt','.time')

# write log-spaced times computed above
np.savetxt(timefn,t,fmt='%.4e')

for j,r in enumerate([0.2032,0.4064,0.6096]):
    # pest template file
    ptffn = '%i.ptf' % (j+1,)
    ptfh = open(ptffn,'w')

    ptfh.write("""ptf &
0  F  8.5D-1  T  T  F                 :: debug (0=quiet,1=some,2=lots), dimensionless?, rtank, wynn epsilon acceleration?, compute flow solution?
%.7e  0.0D+0  1.0D+99   :: pumping rate, pump and recovery time
2.0D-1  4.0D-1  8.0D-2         :: b1, b2, b3
&     Kr       & &   kappa      & &    Ss        & &    Sy        & &   beta       & :: aquifer Kr, kappa, Ss, Sy, and beta
5.0D-2 5.0D-2  5.0D-2  :: sigma for layers 1,2,3
1.0D-4     :: gamma*ell for aquifer
%.6f  3.0D-1                :: Obs loc: r, depth
1.0D-7  1.0D-8  25         :: deHoog parameters
50                         :: finite hankel sum length
F  -6  3  %i  %s   :: calc t?, min log_10(t), max log_10(t), numt, t filename
0.0D+0  0.0D+0  1.0D+0             :: slope, intercept of trend, sign of solution
%i.out
""" % (Q,r,nd,timefn,j+1))
    ptfh.close()

    # pest instruction file
    piffn = '%i.ins' % (j+1,)
    pifh = open(piffn,'w')

    pifh.write('pif *\n*#--------------------------------------*\n')
    for k in range(nd):
        pifh.write('l1 [s%i-%4.4i]89:109\n' % (j+1,k+1))
    pifh.close()

    # pest control file
    pcfh = open(datafile.replace('.txt','.pst'),'w')

pcfh.write("""pcf
* control data
restart estimation
5  %i  1  0  1
3  3  double point  1  0  0
5  2  0.3  0.03  10
3  3  0.001
0.1
30  0.01  3  3  0.01  3
1  1  1
* parameter groups
sppar  relative  0.01  1.00E-20  switch  2.0  parabolic
* parameter data
Kr     none  factor    5.0E-5   1.0E-8   1.0E+8  sppar  1  0  1
kappa  none  factor    0.10     1.0E-8   1.0E+8  sppar  1  0  1
Ss     none  factor    2.1E-2   1.0E-10  1.0     sppar  1  0  1
Sy     none  factor    0.3      1.0E-10  0.5     sppar  1  0  1
beta   none  relative  0.0      0.0      10.0    sppar  1  0  1
* observation groups
spobs
* observation data
""" % (3*nd,))
for k in range(3):
    for j in range(nd):
        weight = 1.0
        pcfh.write('s%i-%4.4i  %.4e  %.2f  spobs\n' % (k+1,j+1,s[j,k],weights[k]))
pcfh.write("""* model command line
./run_head_driver
* model input/output
1.ptf input1.dat
2.ptf input2.dat
3.ptf input3.dat
1.ins 1.out
2.ins 2.out
3.ins 3.out
* prior information
""")
pcfh.close()
