#!/bin/bash 

set -o nounset
set -o errexit

set -x

prefix=SP07082011

##for e in {E021,E022,E023,E024,E025,E026}
for e in {E027,E028,E029,E030,E031}
do
    cd ${prefix}${e}
    #ln -sf ../../sp_driver .
    #ln -sf ../run_sp_driver .
    #ln -sf ../plot_pest_results.py .

    pest ${prefix}${e}corr.pst > /dev/null &

    cd ..
    
done



