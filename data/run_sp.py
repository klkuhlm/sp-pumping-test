import numpy as np
from glob import glob

IN2M = 0.0254 # meters in an inch

t0 = 2940.0 # pumping began (seconds)
t1 = 14806.0 # pumping ended (seconds)

with open('electrodes-configuration.csv','r') as fh:
    lines = fh.readlines()[1:]

elec = {}
for line in lines:
    f = line.strip().split(',')

    # key is electrode (capital E)
    # fields are: electrode, channel, r (inches), depth (inches), angle (degrees CCW from north)
    elec[f[0].upper()] = {'r':float(f[2])*IN2M,
                          'depth':float(f[3])*IN2M}

Q = 0.2/1000.0/60.0 # <- m^3/sec from 0.2 L/min

#test = '08052011'
test = '07082011'

for datafile in glob('SP'+test+'E*.txt'):

    e = datafile.rstrip('.txt').rstrip('corr').lstrip('SP'+test) # electrode name

    # two header lines
    # columns are time (seconds) and SP (volts)
    d = np.loadtxt(datafile,skiprows=2)

    skip = 4
    tmask = d[:,0] < t1
    #tnotmask = d[:,0] > t1
    # only use pumping data
    t = d[tmask,0][::skip]
    phi = d[tmask,1][::skip] 

    initial = d[:20,1].mean() # ~pre-test value

    # value 60-70 minutes after pumping begins
    earlymask = np.logical_and(d[:,0] > t0+60*60, d[:,0] < t0+70*60)  
    earlyval = np.median(d[earlymask,1])

    ##if (initial - earlyval) > 0.0:
    ##    # the signal is a downward pulse
    ##    sign = -1.0
    ##else:
    ##    # the signal is an upward pulse
    ##    sign = 1.0
    sign = 1.0
    print e,sign

    nd = t.shape[0]

    # pest template file
    ptffn = datafile.replace('.txt','.ptf')
    ptfh = open(ptffn,'w')

    outfn = datafile.replace('.txt','.out')
    timefn = datafile.replace('.txt','.time')

    # write log-spaced times computed above
    np.savetxt(timefn,t)

    ptfh.write("""ptf &
0  F  8.5D-1  T  F                  :: debug (0=quiet,1=some,2=lots), dimensionless?, rtank, wynn epsilon acceleration?, compute flow solution?
%.7e  &      t0    & &      t1    &      :: pumping rate, pump and recovery time
2.0D-1  4.0D-1  8.0D-2         :: b1, b2, b3
&     Kr     & &   kappa    & &    Ss      & &    Sy      & &   beta     & :: aquifer Kr, kappa, Ss, Sy, and beta
&  sigma1    & &  sigma2    & &  sigma3    &  :: sigma for layers 1,2,3
&  gamel     &     :: gamma*ell for aquifer
%.6f  %.6f                 :: Obs loc: r, depth
1.0D-7  1.0D-8  25         :: deHoog parameters
50                         :: finite hankel sum length
F  -6  3  %i  %s   :: calc t?, min log_10(t), max log_10(t), numt, t filename
&   growth    & & shift      &  %.1f             :: slope, intercept of trend, sign of solution
%s
""" % (Q,elec[e]['r'],elec[e]['depth'],nd,timefn,sign,outfn))
    ptfh.close()

    # pest instruction file
    piffn = datafile.replace('.txt','.ins')
    pifh = open(piffn,'w')

    pifh.write('pif *\n*#--------------------------------------*\n')
    for j in range(nd):
        # read slope-adjusted phi column
        pifh.write('l1 [phi%4.4i]67:87\n' % (j+1))
    pifh.close()

    # pest control file
    pcfh = open(datafile.replace('.txt','.pst'),'w')

    pcfh.write("""pcf
* control data
restart estimation
13  %i  1  0  1
1  1  double point  1  0  0
5  2  0.3  0.03  10
3  3  0.001
0.1
30  0.01  3  3  0.01  3
1  1  1
* parameter groups
sppar  relative  0.01  1.00E-20  switch  2.0  parabolic
* parameter data
Kr     log   factor    1.2E-5  1.0E-8   1.0E+8  sppar  1  0  1
kappa  log   factor    9.0      1.0E-8   1.0E+8  sppar  1  0  1
Ss     log   factor    6.0E-5   1.0E-10  1.0     sppar  1  0  1
Sy     log   factor    0.1475     1.0E-10  0.5    sppar  1  0  1
beta   fixed relative  0.0      0.0      10.0    sppar  1  0  1
sigma1 log   factor    3.16E-2   1.0E-12  1.0E+4  sppar  1  0  1
sigma2 log   factor    4.89E-2   1.0E-12  1.0E+4  sppar  1  0  1
sigma3 log   factor    4.25E-2   1.0E-12  1.0E+4  sppar  1  0  1
gamel  log   factor    2.063E-3   1.0E-12  1.0E+12 sppar  -1  0  1
growth fixed relative  0.0   0.0      1.0E+8  sppar  1  0  1
shift  fixed relative  %.4e     -1.0E+8  1.0E+8  sppar  1  0  1
t0     none  relative  2940.0   2500.0   3500.0  sppar  1  0  1
t1     fixed relative 14806.8  0.0  189000.0 sppar  1  0  1
* observation groups
spobs
* observation data
""" % (nd,initial))
    for j in range(nd):
        weight = 1.0
        pcfh.write('phi%4.4i  %.8e  %.3f  spobs\n' % (j+1,phi[j],weight))
    pcfh.write("""* model command line
./run_sp_driver
* model input/output
%s sp-finite-input.dat
%s %s
* prior information
""" % (ptffn,piffn,outfn))
    pcfh.close()
