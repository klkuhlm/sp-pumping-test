!
! Copyright (c) 2011 Kristopher L. Kuhlman (klkuhlm at sandia dot gov)
!
! Permission is hereby granted, free of charge, to any person obtaining a copy
! of this software and associated documentation files (the "Software"), to deal
! in the Software without restriction, including without limitation the rights
! to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
! copies of the Software, and to permit persons to whom the Software is
! furnished to do so, subject to the following conditions:
!
! The above copyright notice and this permission notice shall be included in
! all copies or substantial portions of the Software.
!
! THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
! IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
! FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
! AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
! LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
! OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
! THE SOFTWARE.
!
! Malama, B., A. Revil, and K.L. Kuhlman, 2009. A semi-analytical solution
!  for transient streaming potentials associated with confined aquifer
!  pumping tests, Geophysical Journal International, 176(3), 1007–1016.
! http://dx.doi.org/10.1111/j.1365-246X.2008.04014.x
!
! Malama, B., K.L. Kuhlman, and A. Revil, 2009. Theory of transient
!  streaming potentials associated with axial-symmetric flow in unconfined
!  aquifers, Geophysical Journal International, 179(2), 990–1003.
! http://dx.doi.org/10.1111/j.1365-246X.2009.04336.x
!

Program Driver

  ! constants and coefficients
  use constants, only : DP, EP, PIEP

  use invLap, only : invLaplace, deHoog_invlap, deHoog_pvalues

  use three_layer_finite, only : unconfined_aquifer_sp, neuman72

  implicit none

  real(EP), allocatable :: j0zero(:)
  real(DP) :: x, dx  ! <-- reduce to double-precision for ifort; it does not have quad-precision bessel functions
  type(invLaplace) :: lap
  character(75) :: outfile, infile, tfile
  integer :: i,j, numt, minlogt, maxlogt, debug
  real(EP), allocatable  :: J0J1(:,:)
  real(DP), allocatable :: ts(:), td(:)
  complex(EP), allocatable :: fa(:,:), na(:,:), p(:), pmult(:), fsum(:), nsum(:)
  real(EP) :: phi, dphi, head, dhead
  real(DP) :: r, depth, rtank, rtD, Q, tee, growth, shift, signphi
  real(DP), dimension(0:1) :: tp, tDp
  real(EP), dimension(5) :: out

  integer :: nj,np,ioerr

  ! realted to dimensional solution
  logical :: dimless, calct, wynn, flow, spcalc
  real(EP) :: HC, PhiC

  ! aquifer parameters (primary - read from input)
  real(DP), dimension(3) :: b,sigma
  real(DP) :: Kr,Kz,Ss,Sy,zd,rd,beta,gamell

  ! aquifer parameters (secondary - computed from above)
  real(DP) :: alphaR, alphaY, alphaD, kappa, betaD, varTheta
  real(DP), dimension(3) :: sigmaD

  real(DP) :: bd1, bd3
  real(EP) :: initialphi
  logical, dimension(3) :: layer

  intrinsic :: bessel_j0, bessel_j1

  ! $$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$
  infile = 'sp-finite-input.dat'
  open(unit=19, file=infile, status='old', action='read')

  ! suppress output to screen, radius of tank (distance to BC)
  read(19,*) debug, dimless, rtank, wynn, flow

  backspace(19)
  read(19,*,iostat=ioerr) debug, dimless, rtank, wynn, flow, spcalc
  ! if no entry, assume you want to calc SP
  if(ioerr /= 0) then
     spcalc = .true.
  end if


  read(19,*) Q, tp(0:1)  ! pumping rate, beginning of pumping and recovery (time)
  read(19,*) b(1:3) ! layer thicknesses
  read(19,*) Kr, kappa, Ss, Sy, beta
  read(19,*) sigma(1:3)
  read(19,*) gamell  !! rho*g*ell = gamma*ell = C (only used to re-dimensionalize)
  read(19,*) r,depth ! x,y,depth of observation point

  if (debug > 0) then
     write(*,'(A,I0,A,L1,A,ES10.3,2(A,L1))') 'debug level:', debug, '  dimensionless?',dimless, &
          & ' r_{tank}',rtank, ' Wynns epsilon acceleration on finite Hankel?', wynn, &
          & ' compute flow solution if in layer 2?', flow
     write(*,111) 'layer b:',b(1:3)
     write(*,'(A,4(1X,ES10.3))') 'Kr,kappa,Ss,Sy:', Kr, kappa, Ss, Sy, beta
     write(*,111) 'sigma:',sigma(1:3)
     write(*,111) 'gamma*ell:',gamell
     write(*,'(A,2(ES10.3,1X))') 'obs r,depth:',r,depth
  end if

111 format(A,3(1X,ES10.3))

  ! need some error-checking for electrical parameters
  if(any((/b(:),Kr,kappa,Ss,Sy,sigma(:),r,rtank/) < spacing(0.0))) then
     print *, 'input error: zero or negative aquifer parameters'
     stop
  end if

  if (tp(1)-tp(0) < spacing(0.0) .or. tp(0) < -spacing(0.0)) then
     print *, 'start and stop times for pumping must satisfy'
     print *, 'the relation 0 <= t0 < t1',tp(0),tp(1)
     stop
  end if

  ! dimensionless thicknesses
  bd1 = b(1)/b(2)
  bd3 = b(3)/b(2)

  ! non-dimensionalize all distances by aquifer thickness (second layer)
  rD = r/b(2)
  rtD = rtank/b(2)

  if (rD > rtD) then
     print *, 'observation loction must be in the interval 0 < r <= r_tank',r,rtank
     stop
  end if

  ! zd is positive up from bottom of aquifer; depth down from surface
  zd = 1.0_DP + (b(1) - depth)/b(2)

  if (depth > sum(b(1:3)) .or. depth < 0.0) then
     print *, 'although z coordinate is up from bottom of aquifer (-b(3) <= z <= sum(b(1:2)))'
     print *, 'depth is positive, going down from surface, and it'
     print *, 'must be in the interval 0 <= depth <= sum(b(1:3))',depth,b(1:3)
     stop
  end if

  ! perform some error checking on coordinates
  !%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
  layer(:) = .false.

  if(zd > bd1 + 1.0 .or. zd < -bd3) then
     print *, 'input error: z coordinate out of range'
     stop
  elseif(zd >= 1.0) then ! upper unpumped vadose zone
     layer(1) = .true.
     if(debug > 1) then
        print *, 'observation point in vadose zone above unconfined layer'
     end if
  elseif(zd > 0.0) then  ! middle pumped unconfined aquifer
     layer(2) = .true.
     if(debug > 1) then
        print *, 'observation point in pumped unconfined aquifer'
     end if
  else                   ! lower unpumped aquiclude
     layer(3) = .true.
     if(debug > 1) then
        print *, 'observation point in lower unpumped aquiclude'
     end if
  end if

  read(19,*) lap%alpha, lap%tol, lap%M
  np = 2*lap%M+1
  read(19,*) nj
  if(nj < 1) then
     print *, 'Finite Hankel sum order must be >= 1',nj
     stop
  end if

  read(19,*) calct, minlogt, maxlogt, numt, tfile

  if (debug > 1) then
     print *, 'finite Hankel sum length',nj
     print *, 'calc t?',calct,' min/max log10(t)',minlogt,maxlogt,' numt',&
          & numt,' tfile',trim(tfile)
  end if

  allocate(ts(numt), td(numt), j0zero(nj), p(np), &
       & J0J1(nj,np), fa(nj,np),na(nj,np), pmult(np),fsum(np),nsum(np))

  if (.not. calct) then
     open(unit=22,file=tfile,action='read',status='old')
     do i=1,numt
        ! times are in a file one per line
        read(22,*) ts(i)
     end do
     close(22)
  else
     ! compute times from log-uniform vector
     ts(1:numt) = logspace(minlogt,maxlogt,numt)
  end if

  read(19,*) growth, shift, signphi
  read(19,*) outfile
  close(19)

  if(any(ts <= spacing(0.0))) then
     print *, 'all times must be > 0', ts
     stop
  end if

  ! force sign to be just -1 or 1 (but not zero!)
  if(signphi > 0) then
     signphi = 1.0_EP
  else
     signphi = -1.0_EP
  end if

  alphaR = Kr/Ss
  Kz = kappa*Kr
  alphaY = b(2)*Kz/Sy
  alphaD = alphaY/alphaR
  varTheta = Sy/(b(2)*Ss)
  betaD = beta/b(2)
  sigmaD(1:3) = [ sigma(1)/sigma(2),1.0_DP,sigma(3)/sigma(2) ]
  td(1:numt) = alphaR*ts(1:numt)/b(2)**2
  tdp(0:1) = alphaR*tp(0:1)/b(2)**2

  if (debug > 0) then
     write(*,333) 'alpha_r:',alphaR
     write(*,333) 'K_z:', Kz
     write(*,333) 'alpha_y:',alphaY
     write(*,333) 'alpha_D:',alphaD
     write(*,333) 'varTheta:',varTheta
     write(*,333) 'beta_D:',betaD
     write(*,'(A,3(1X,ES10.3))') 'sigma_D:',sigmaD
     write(*,333) 'b_{D,1}:',bd1
     write(*,333) 'b_{D,3}:',bd3
     write(*,'(A,3(ES10.3,1X))') 'zD,rD,rtD',zD,rD,rtD
333  format(A,ES10.3)
  end if

  ! compute zeros of J0(rtd*x)
  !************************************
  ! asymptotic estimate of zeros - initial guess
  j0zero = [((real(i-1,DP) + 0.75_DP)*PIEP, i=1,nj)]
  do i=1,nj
     x = j0zero(i)
     NR: do
        ! Newton-Raphson
        dx = bessel_j0(x)/bessel_j1(x)
        x = x + dx
        if(abs(dx) < spacing(x)) then
           exit NR
        end if
     end do NR
     ! convert zeros of J0(x) -> J0(rtd*x)
     j0zero(i) = x/rtD
  end do

  if (debug > 1) then
     print *, 'j0zero',nj,j0zero
  end if

  open(unit=20,file=outfile,action='write',status='replace')

  ! echo input parameters into output file
  write (20,'(3(A,L2))') '# dimensionless output?:',dimless, &
       & " Wynn's epsilon acceleration on finite Hankel?", wynn, " compute flow solution?", flow 
  write (20,'(1(A,ES12.5))') '# radius of tank :',rtank
  write (20,'(A,1(ES12.5,1X))') '# pumping rate Q', Q
  write (20,'(2(A,ES12.5,1X))') '# pumping start:', tp(0),'pumping end:',tp(1)
  write (20,'(A,3(1X,ES12.5))') '# b (vadose,pumped,aquatard):',b(1:3)
  write (20,'(A,3(1X,ES12.5))') '# sigma:',sigma(1:3)
  write (20,'(A,3(1X,ES12.5))') '# gamma*ell:',gamell
  write (20,'(2(A,ES12.5))') '# Ss:',Ss, ' Kr:',Kr
  write (20,'(3(A,ES12.5))') '# Sy:',Sy, ' Kz:',Kz, ' beta:',beta
  write (20,'(4(A,ES12.5))') '# obs zD:',zd, ' rD:',rd, ' obs depth:',depth,' rtD:',rtd
  write (20,'(2(A,ES12.5,1X),A,I0)') '# deHoog alpha:',lap%alpha,'tol:',lap%tol,'M: ',lap%M
  write (20,'(A,I0)') '# finite Hankel terms: ',nj
  write (20,'(3(A,ES12.5,1X))') '# exp growth:',growth, ' shift:',shift, ' sign:',signphi
  write (20,'(A)') '#'

  if (dimless) then
     write (20,'(A)') '#         t_D       ' &
          &//'        phi_D             log-deriv phi_D      adjusted phi_D'&
          &//'         s_D               log-deriv s_D'
  else
     write (20,'(A)') '#         t         ' &
          &//'        phi               log-deriv phi         adjusted phi'&
          &//'         s                 log-deriv s'
  end if
  write (20,'(A)') '#----------------------------'&
       &//'---------------------------------------'&
       &//'-------------------------------------------------------'

  ! combine solutions and re-dimensionalize
  HC = Q/(4.0*PIEP*b(2)*Kr)
  PhiC = gamell*HC/sigma(2)

  if (debug > 0) then
     print *, 'Hc', HC
     print *, 'PhiC',PhiC
  end if

  ! kernel of finite Hankel transform
  ! arguments of bessel functions converted to double precision to accomidate ifort
  J0J1(1:nj,1:np) = 2.0/rtD**2 * &
       & spread(bessel_j0(real(j0zero(:)*rD,DP))/bessel_j1(real(j0zero(:)*rtD,DP))**2,2,np)

  do i = 1, numt

     tee = 2.0*tD(i)
     p(1:np) = deHoog_pvalues(tee,lap)

     pmult(1:np) = exp(-tdp(0)*p(1:np))/p(1:np) - exp(-tdp(1)*p(1:np))/p(1:np)

     if(debug > 1) then
        print *, 'tD',tD(i), 'p',p
     end if

     ! compute laplace-finite hankel space SP solution
     if (spcalc) then
        fa(1:nj,1:np) = unconfined_aquifer_sp(j0zero(1:nj),p(1:np),pmult(1:np),zd,bd1,bd3,&
             & layer(1:3),sigmaD(1:3),kappa,varTheta,betaD)
     end if

     ! flow solution only makes sense in layer 2
     if (layer(2) .and. flow) then
        na(1:nj,1:np) = neuman72(p(1:np),j0zero(1:nj),pmult(1:np),zd,kappa,alphaD,betaD)
     end if

     ! invert solution and ln(t) derivative
     if (wynn) then
        do j=1,np
           if (spcalc) then
              ! finite hankel transform sum accelerated
              fsum(j) = wynn_epsilon(fa(1:nj,j)*J0J1(1:nj,j),debug)
           end if

           if (layer(2) .and. flow) then
              nsum(j) = wynn_epsilon(na(1:nj,j)*J0J1(1:nj,j),debug)
           end if
        end do
     else
        if (spcalc) then
           fsum(1:np) = sum(fa(1:nj,1:np)*J0J1(1:nj,1:np),dim=1)
        end if

        if (layer(2) .and. flow) then
           nsum(1:np) = sum(na(1:nj,1:np)*J0J1(1:nj,1:np),dim=1)
        end if
     end if

     if (spcalc) then
        phi =  signphi*deHoog_invlap(tD(i),tee,fsum(:),lap)
        dphi = signphi*deHoog_invlap(tD(i),tee,fsum(:)*p(:),lap)*tD(i)
     else
        phi = -7.0
        dphi = -8.0
     end if

     if (layer(2) .and. flow) then
        head =  deHoog_invlap(tD(i),tee,nsum(:),lap)
        dhead = deHoog_invlap(tD(i),tee,nsum(:)*p(:),lap)*tD(i)
     else
        head = -1.0
        dhead = -1.0
     end if

     if (i == 1) then
        initialphi = phi
     end if

     if(dimless) then
        out(1:5) = [phi, dphi, phi-initialphi, head, dhead]
        write(20,'(6(ES21.12E4,1X))') tD(i), out(1:5)
     else
        out(1:5) = [PhiC*phi, PhiC*dphi, PhiC*(phi-initialphi), head*HC, dhead*HC]
        write(20,'(6(ES21.12E4,1X))') ts(i), out(1:5)
     end if

  end do

  close(20)

contains



  !! ###################################################
  !! wynn-epsilon acceleration of partial sums, given a series
  !! all intermediate sums / differences are done in extended precision
  function wynn_epsilon(series,quiet) result(accsum)
    use constants, only : EP
    implicit none

    integer, parameter :: MINTERMS = 4
    complex(EP), dimension(:), intent(in) :: series
    integer, intent(in) :: quiet
    complex(EP) :: accsum, denom
    integer :: ns, i, j, m
    complex(EP), dimension(1:size(series),-1:size(series)-1) :: eps

    ns = size(series)

    ! build up partial sums, but check for problems
    check: do i=1,ns
       if (.not. is_finite(series(i))) then
          ns = i-1
          if(ns < MINTERMS) then
             if (quiet > 1) then
                write(*,'(A)',advance='no') 'not enough Wynn-Epsilon series to accelerate '
             end if
             accsum = -999999.9  ! make it clear answer is bogus
             goto 777
          else
             if (quiet > 1) then
                write(*,'(A,I3,A)',advance='no') 'Wynn-Epsilon series&
                     &, truncated to ',ns,' terms. '
             end if
             exit check
          end if
       else
          ! term is good, continue
          eps(i,0) = sum(series(1:i))
       end if
    end do check

    ! first column is intiallized to zero
    eps(:,-1) = 0.0_EP

    ! build up epsilon table (each column has one less entry)
    do j = 0,ns-2
       do m = 1,ns-(j+1)
          denom = eps(m+1,j) - eps(m,j)
          if(abs(denom) > epsilon(abs(denom))) then ! check for div by zero
             eps(m,j+1) = eps(m+1,j-1) + 1.0_EP/denom
          else
             accsum = eps(m+1,j)
             if (quiet > 1) then
                write(*,'(A,I0,1X,I0,A)') 'epsilon cancel ',m,j,':'
             end if
             goto 777
          end if
       end do
    end do

    ! if made all the way through table use "corner value" of triangle as answer
    if(mod(ns,2) == 0) then
       accsum = eps(2,ns-2)  ! even number of terms - corner is acclerated value
    else
       accsum = eps(2,ns-3)  ! odd numbers, use one column in from corner
    end if
777 continue
  end function wynn_epsilon

  elemental function is_finite(x) result(pred)
    use constants, only : EP
    complex(EP), intent(in) :: x
    logical :: pred
    pred = .not. (x /= x .or. abs(x) > huge(abs(x)))
  end function is_finite

  elemental function is_bad(x) result(pred)
    use constants, only : EP
    real(EP), intent(in) :: x
    logical :: pred
    intrinsic :: isnan
    pred = (isnan(x) .or. x > huge(x))
  end function is_bad

  function linspace(lo,hi,num) result(v)
    use constants, only : DP
    real(DP), intent(in) :: lo,hi
    integer, intent(in) :: num
    real(DP), dimension(num) :: v
    integer :: i
    real(DP) :: rnum, range, sgn

    rnum = real(num - 1,DP)
    range = abs(hi - lo)
    sgn = sign(1.0_DP,hi-lo) ! if lo > high, count backwards
    forall (i=0:num-1)
       v(i+1) = lo + sgn*real(i,DP)*range/rnum
    end forall
  end function linspace

  function logspace(lo,hi,num) result(v)
    use constants, only : DP
    integer, intent(in) :: lo,hi,num
    real(DP), dimension(num) :: v
    v = 10.0_DP**linspace(real(lo,DP),real(hi,DP),num)
  end function logspace

end program Driver
