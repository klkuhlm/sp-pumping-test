!
! Copyright (c) 2008 Kristopher L. Kuhlman (kuhlman at hwr dot arizona dot edu)
!
! Permission is hereby granted, free of charge, to any person obtaining a copy
! of this software and associated documentation files (the "Software"), to deal
! in the Software without restriction, including without limitation the rights
! to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
! copies of the Software, and to permit persons to whom the Software is
! furnished to do so, subject to the following conditions:
!
! The above copyright notice and this permission notice shall be included in
! all copies or substantial portions of the Software.
!
! THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
! IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
! FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
! AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
! LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
! OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
! THE SOFTWARE.
!
! Malama, B., A. Revil, and K.L. Kuhlman, 2009. A semi-analytical solution
!  for transient streaming potentials associated with confined aquifer
!  pumping tests, Geophysical Journal International, 176(3), 1007–1016.
! http://dx.doi.org/10.1111/j.1365-246X.2008.04014.x
!
! Malama, B., K.L. Kuhlman, and A. Revil, 2009. Theory of transient
!  streaming potentials associated with axial-symmetric flow in unconfined
!  aquifers, Geophysical Journal International, 179(2), 990–1003.
! http://dx.doi.org/10.1111/j.1365-246X.2009.04336.x
!

Program Driver

  ! constants and coefficients
  use constants, only : DP, EP, PI

  use invLap, only : invLaplace, deHoog_invlap, deHoog_pvalues

  use three_layer_infinite

  implicit none

  real(EP), allocatable :: j0zero(:)
  real(EP) :: x, dx
  type(invLaplace) :: lap
  character(23) :: fmt
  character(75) :: outfile, infile
  integer :: i, j, w, numt, terms, minlogsplit, maxlogsplit
  integer :: debug, splitrange, zerorange
  real(DP), allocatable :: GLx(:),GLw(:), GLy(:), GLz(:)

  ! vectors of results and intermediate steps
  integer, allocatable :: splitv(:)
  real(DP), allocatable  :: ts(:), td(:), dt(:) , fa(:)
  complex(EP), allocatable :: finOneAqif(:,:), infOneAqif(:,:), totOneAqif(:,:)
  complex(EP), allocatable :: oneAqifD(:), deriv(:)

  real(DP) :: lob,hib,width
  real(DP), allocatable :: area(:)

  !! k parameter in tanh-sinh quadrature (order is 2^k - 1)
  integer :: tsk, tsN, nst
  real(DP), allocatable :: tsw(:), tsa(:), sgn(:), tshh(:), tmp(:)
  integer, allocatable :: tskk(:), tsNN(:), ii(:)
  integer :: GLorder, j0split(1:2), naccel, err, m

  ! realted to dimensional solution
  logical :: quiet, dipole
  real(DP) :: HC, PhiC, Qrate, PolErr

  intrinsic :: bessel_j0, bessel_j1

  ! $$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$
  ! read input parameters from file some minimal error checking
  infile = 'input.dat'
  open(unit=19, file=infile, status='old', action='read')

  ! suppress output to screen, T/F dipole test, integer BC type,
  ! perpendicular distance to boundary from pumping well
  read(19,*) debug
  read(19,*) Qrate  ! injection is -Qrate, pumping is +Qrate
  read(19,*) b(1:3) ! layer thicknesses
  read(19,*) Kr, kappa, Ss, Sy
  read(19,*) sigma(1:3)
  read(19,*) gamell(1:3)  !! rho*g*ell = gamma*ell = C (only used to re-dimensionalize)
  read(19,*) numt
  read(19,*) obs(1:2),depth ! x,y,depth of observation point

  if (debug > 0) then
     write(*,'(A,I1)') 'debug level:',debug
     write(*,111) 'layer b:',b(1:3)
     write(*,'(A,4(1X,ES10.3))') 'Kr,kappa,Ss,Sy:', Kr, kappa, Ss, Sy
     write(*,111) 'sigma:',sigma(1:3)
     write(*,111) 'gamma*ell:',gamell(1:3)
     write(*,'(A,1X,I4)') '# times',numt
     write(*,'(A,ES10.3)') 'obs depth:',depth
  end if

111 format(A,3(1X,ES10.3))
222 format(2(A,ES10.3),A)

  ! need some error-checking for electrical parameters
  if(any((/b(:),Kr,kappa,Ss,Sy,sigma(:)/) < SMALL)) then
     print *, 'input error: zero or negative aquifer parameters'
     stop
  end if

  ! dimensionless thicknesses
  bd1 = b(1)/b(2)
  bd3 = b(3)/b(2)

  sgn(:) = -999.99

  ! non-dimensionalize all distances by aquifer thickness (second layer)
  rD = r/b(2)

  ! zd is positive up from bottom of aquifer; depth down from surface
  zd = 1.0_DP + (b(1) - depth)/b(2)

  ! perform some error checking on coordinates
  !%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
  layer = .false.

  if(zd > bd1 + 1.0 .or. zd < -bd3) then
     print *, 'input error: z coordinate out of range'
     stop
  elseif(zd >= 1.0) then ! upper unpumped vadose zone
     layer(1) = .true.
     if(debug > 1) then
        print *, 'observation point in vadose zone above unconfined layer'
     end if
  elseif(zd > 0.0) then  ! middle pumped unconfined aquifer
     layer(2) = .true.
     if(debug > 1) then
        print *, 'observation point in pumped unconfined aquifer'
     end if
  else                   ! lower unpumped aquiclude
     layer(3) = .true.
     if(debug > 1) then
        print *, 'observation point in lower unpumped aquiclude'
     end if
  end if

  read(19,*) lap%alpha, lap%tol, lap%M
  np = 2*lap%M+1
  read(19,*) tsk, j0split(1:2), naccel, GLorder, nst

  if(tsk - nst < 2) then
     print *, 'Tanh-Sinh k is too low (',tsk,') for given level&
          & of Richardson extrapolation (',nst,').  Increase tsk or decrease nst.'
     stop
  end if

  if(any((/j0split(:),naccel, tsk/) < 1)) then
     print *, 'max/min split, # accelerated terms and tanh-sinh k must be >= 1'
     stop
  end if

  ! solution vectors
  allocate(oneAqifD(numt), finOneAqif(numt), infOneAqif(numt))
  allocate(deriv(numt), totOneAqif(numt))
  allocate(ts(numt), td(numt), splitv(numt), dt(numt))

  terms = maxval(j0split(:))+naccel+1
  allocate(j0zero(terms))

  ! time is now read as a column rather than a long row,
  ! to accomidate the fact that PEST has a maximum row length
  do i=1,numt
     read(19,*) ts(i)
  end do

  read (19,*) outfile
  if(any(ts <= SMALL)) then
     print *, 'all times must be > 0'
     stop
  end if

  alphaR = Kr/Ss
  Kz = kappa*Kr
  alphaY = b(2)*Kz/Sy
  alphaD = alphaY/alphaR
  sigmaD(1:3) = [ sigma(1)/sigma(2),1.0_DP,sigma(3)/sigma(2) ]
  td(1:numt) = alphaR*ts(1:numt)/b(2)**2

if (debug > 1) then
  write(*,333) 'alpha_r:',alphaR
  write(*,333) 'K_z:', Kz
  write(*,333) 'alpha_y:',alphaY
  write(*,333) 'alpha_D:',alphaD
  write(*,'(A,3(1X,ES10.3))') 'sigma_D:',sigmaD
  write(*,333) 'b_{D,1}:',bd1
  write(*,333) 'b_{D,3}:',bd3
333 format(A,ES10.3)
end if

  ! compute zeros of J0 bessel function
  !************************************
  ! asymptotic estimate of zeros - initial guess
  j0zero = (/((real(i-1,DP) + 0.75_DP)*PI,i=1,terms)/)
  do i=1,TERMS
     x = j0zero(i)
     NR: do
        ! Newton-Raphson
        dx = bessel_j0(x)/bessel_j1(x)
        x = x + dx
        if(abs(dx) < spacing(x)) then
           exit NR
        end if
     end do NR
     j0zero(i) = x
  end do

  ! split between finite/infinite part should be
  ! small for large time, large for small time
  do i=1,numt
     zerorange = maxval(j0split(:)) - minval(j0split(:))
     minlogsplit = floor(minval(log10(td)))   ! min log(td) -> maximum split
     maxlogsplit = ceiling(maxval(log10(td))) ! max log(td) -> minimum split
     splitrange = maxlogsplit - minlogsplit + 1
     splitv(i) = minval(j0split(:)) + &
          & int(zerorange*((maxlogsplit - log10(td(i)))/splitrange))
  end do

if (debug > 1) then
  print *, 'computed splits (integer zeros): ',splitv(:)
  print *, 'location of splits (zeros of J0):'
  do i= minval(splitv(:)),maxval(splitv(:))
     print *, i,j0zero(i)
  end do
end if

!$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$
! finite portion of Hankel integral for each time level

if(debug > 1) then
   write(*,*) 'integrate finite portion:'
   write(*,*) '=========================='
end if

if(.not. allocated(tsw)) then
   tsN = 2**tsk - 1
   allocate(tsw(tsN),tsa(tsN),fa(tsN),ii(tsN))
   allocate(tskk(nst),tsNN(nst),tshh(nst),tmp(nst))
end if

do i = 1, numt
   if(debug > 1) then
      if (i == 1) then
         write (*,888) 'upper bound: ',j0zero(splitv(1))/rD
      elseif(splitv(i) /= splitv(i-1)) then
         write (*,888) 'upper bound: ',j0zero(splitv(i))/rD
      end if
   end if

   tee = 2.0*tD(i)
   p(1:np) = deHoog_pvalues(tee,lap)
   pmult(1:np) = exp(-tdp(0)*p(1:np))/p(1:np) - exp(-tdp(1)*p(1:np))/p(1:np)

   ! three-layer unconfined electrokinetic system
   !===========================

   tskk(1:nst) = (/ (tsk-m,m=nst-1,0,-1)/)
   tsNN(1:nst) = 2**tskk - 1
   tshh(1:nst) = 4.0_DP/(2**tskk)

   ! compute abcissa and for highest-order case (others are subsets)
   call tanh_sinh_setup(tskk(nst),j0zero(splitv(i))/rD,&
        & tsw(1:tsNN(nst)),tsa(1:tsNN(nst)))

   ! compute solution at densest set of abcissa
   pfa(1:nj,1:np) = unconfined_aquifer_sp(j0zero(1:nj),p(1:np),zd,bd1,bd3,&
        & layer(1:3),sigmaD(1:3),kappa,alphaD)

   !$OMP BEGIN PARALLEL DO
   do j=1:nj
      fa(j)= deHoog_invlap(tD(i),tee,pmult(:)*pfa(j,:),lap)
   end do
   !$OMP END PARALLEL DO

   tmp(nst) = (j0zero(splitv(i))/rD)/2.0*sum(tsw(:)*fa(:))

   do j=nst-1,1,-1
      !  only need to re-compute weights for each subsequent step
      call tanh_sinh_setup(tskk(j),j0zero(splitv(i))/rD,&
           & tsw(1:tsNN(j)),tsa(1:tsNN(j)))

      ! compute index vector, to slice up solution
      ! for nst'th turn count regular integers
      ! for (nst-1)'th turn count even integers
      ! for (nst-2)'th turn count every 4th integer, etc...
      ii(1:tsNN(j)) = (/( m* 2**(nst-j), m=1,tsNN(j) )/)

      ! estimate integral with subset of function evaluations and appropriate weights
      tmp(j) = (j0zero(splitv(i))/rD)/2.0*sum(tsw(1:tsNN(j))*fa(ii(1:tsNN(j))))
   end do

   if (nst > 1) then
      call polint(tshh(1:nst),tmp(1:nst),0.0_DP,finOneAqif(j),PolErr)
   else
      finOneAqif(j) = tmp(1)
   end if

   if(debug > 1) then
      write(*,777) 'tD=', td(i),' ',finOneAqif(i)
   end if

end do
777 format(A,ES8.2,A,ES14.6E3)
888 format(A,ES9.2)

!$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$
! "infinite" portion of Hankel  integral for each time level
! integrate between zeros of Bessel function, extrapolate
! area from series of areas of alternating sign

if(debug > 1) then
   write(*,*)
   write (*,*) 'integrate infinite portion'
   write(*,*) '============================='
end if

if(.not. allocated(GLx)) then
   allocate(Glx(GLorder-2),GLw(GLorder-2),area(naccel))
   call gauss_lobatto_setup(GLorder,GLx,GLw)  ! get weights and abcissa
end if

do i = 1, numt

   if(debug > 1) then
      if (i == 1) then
         write(*,888) 'lower bound:', j0zero(splitv(1))/rD
      elseif(splitv(i) /= splitv(i-1)) then
         write(*,888) 'lower bound:', j0zero(splitv(i))/rD
      end if
   end if

   do j = splitv(i)+1, splitv(i)+naccel
       lob = j0zero(j-1)/rd ! lower bound
       hib = j0zero(j)/rd   ! upper bound
       width = hib - lob

       ! transform GL abcissa to global coordinates
       GLy(:) = (width*GLx(:) + (hib + lob))/2.0
       pfa(:) = unconfined_aquifer_sp(GLy(1:glorder-1),p(1:np),zd,bd1,bd3,&
            & layer(1:3),sigmaD(1:3),kappa,alphaD)

       !$OMP BEGIN PARALLEL DO
       do j=1:nj
          GLz(j)= deHoog_invlap(tD(i),tee,pmult(:)*pfa(j,:),lap)
       end do
       !$OMP END PARALLEL DO

       if (debug > 0) then
          if (any(GLz /= GLz)) then
             print *, 'NaN in GLz'
          elseif (any(abs(GLz) >= huge(1.0D1))) then
             print *, '+/- infinity in GLz'
          end if
       end if

       area(j-splitv(i)) = width/2.0*sum(GLz(1:glorder-2)*GLw(1:glorder-2))
    end do

    fsum(:) = wynn_epsilon(area(1:naccel))

    if(debug > 1) then
      write(*,777) 'tD=', td(i),' ',infOneAqif(i,w)
   end if

end do

! combine solutions and re-dimensionalize
HC = Qrate/(4.0_DP*PI*b(2)*Kr)
PhiC = gamell(2)*HC/sigma(2)
totOneAqif(1:numt,1:npts) = (finOneAqif(:,:) + infOneAqif(:,:))*PhiC
oneAqifD = 0.0

! $$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$
! write results to file

! open output file or die
open (unit=20, file=outfile, status='replace', action='write', iostat=err)
if (err /= 0) then
   print *, 'cannot open output file for writing'
   stop
end if

! echo input parameters
write (20,'(A,1(ES10.3,1X))') '# pumping rate Q', Qrate
write (20,'(A,3(1X,ES10.3))') '# b:',b(1:3)
write (20,'(A,3(1X,ES10.3))') '# sigma:',sigma(1:3)
write (20,'(A,3(1X,ES10.3))') '# gamma*ell:',gamell(1:3)
write (20,'(2(A,ES10.3))') '# Ss:',Ss, ' Kr:',Kr
write (20,'(2(A,ES10.3))') '# Sy:',Sy, ' Kz:',Kz
write (20,'(3(A,ES10.3))') '# obs zd:',zd
fmt = '(A,I2,A,  (ES10.3,1X))'
write (20,'(A,I4)') '# tanh-sinh quadrature order:',tsN
write (20,'(A,2I3,ES10.3,A,I4)') &
     &'# min/max J0(x) zero split:', minval(j0split(:)),maxval(j0split(:)), &
     & j0zero(splitv(1)), ' # 0s to accelerate:', naccel
write (20,'(A,I2)') '# order of Gauss-Lobatto quad:', GLorder
write (20,'(A)') '#'

write (20,'(A)') '#         t_D          &
     &      H^{-1}[ L^{-1}[ f_D ]]           &
     &   log-deriv          '
write (20,'(A)') '#----------------------------&
     &------------------------------------------------'

do i = 1, numt
   write (20,'(3(1x,ES24.15E3))') ts(i), oneAqifD(i), deriv(i)
end do

contains

  subroutine tanh_sinh_setup(k,s,w,a)
    use constants, only : PIOV2
    implicit none

    integer, intent(in) :: k
    real(DP), intent(in) :: s
    real(DP), intent(out), dimension(2**k-1) :: w, a

    integer :: N,r,i
    real(DP) :: h
    real(DP), allocatable :: u(:,:)

    !! compute weights not saved as constants above
    N = 2**k-1
    r = (N-1)/2
    h = 4.0_DP/2**k
    allocate(u(2,N))

    u(1,1:N) = PIOV2*cosh(h*(/(i,i=-r,r)/))
    u(2,1:N) = PIOV2*sinh(h*(/(i,i=-r,r)/))

    a(1:N) = tanh(u(2,1:N))
    w(1:N) = u(1,1:N)/cosh(u(2,:))**2
    w(1:N) = 2.0_DP*w(1:N)/sum(w(1:N))

    ! map the -1<=x<=1 interval onto 0<=a<=s
    a = (a + 1.0_DP)*s/2.0_DP

  end subroutine tanh_sinh_setup


  !! ###################################################
  subroutine gauss_lobatto_setup(ord,abcissa,weight)
    implicit none
    integer, intent(in) :: ord
    real(DP), intent(out), dimension(ord-2) :: weight, abcissa

    ! leave out the endpoints (abcissa = +1 & -1), since
    ! they will be the zeros of the Bessel functions
    ! (therefore, e.g., 5th order integration only uses 3 points)
    ! copied from output of Matlab routine by Greg von Winckel, at
    ! http://www.mathworks.com/matlabcentral/fileexchange/loadFile.do?objectId=4775

    select case(ord)
    case(5)
       abcissa(1:2) = (/-0.65465367070798_DP, 0.0_DP/)
       abcissa(3) = -abcissa(1)
       weight(1:2) =   (/0.54444444444444_DP, 0.71111111111111_DP/)
       weight(3) = weight(1)
    case(6)
       abcissa(1:2) = (/-0.76505532392946_DP, -0.28523151648065_DP/)
       abcissa(3:4) = -abcissa(2:1:-1)
       weight(1:2) = (/0.37847495629785_DP, 0.55485837703549_DP/)
       weight(3:4) = weight(2:1:-1)
    case(7)
       abcissa(1:3) = (/-0.83022389627857_DP, -0.46884879347071_DP, 0.0_DP/)
       abcissa(4:5) = -abcissa(2:1:-1)
       weight(1:3) = (/0.27682604736157_DP, 0.43174538120986_DP, &
            & 0.48761904761905_DP/)
       weight(4:5) = weight(2:1:-1)
    case(8)
       abcissa(1:3) = (/-0.87174014850961_DP, -0.59170018143314_DP, &
            & -0.20929921790248_DP/)
       abcissa(4:6) = -abcissa(3:1:-1)
       weight(1:3) = (/0.21070422714351_DP, 0.34112269248350_DP, &
            & 0.41245879465870_DP/)
       weight(4:6) = weight(3:1:-1)
    case(9)
       abcissa(1:4) = (/-0.89975799541146_DP, -0.67718627951074_DP, &
                      & -0.36311746382618_DP, 0.0_DP/)
       abcissa(5:7) = -abcissa(3:1:-1)
       weight(1:4) = (/0.16549536156081_DP, 0.27453871250016_DP, &
            & 0.34642851097305_DP, 0.37151927437642_DP/)
       weight(5:7) = weight(3:1:-1)
    case(10)
       abcissa(1:4) = (/-0.91953390816646_DP, -0.73877386510551_DP, &
            & -0.47792494981044_DP, -0.16527895766639_DP/)
       abcissa(5:8) = -abcissa(4:1:-1)
       weight(1:4) = (/0.13330599085107_DP, 0.22488934206313_DP, &
            & 0.29204268367968_DP, 0.32753976118390_DP/)
       weight(5:8) = weight(4:1:-1)
    case(11)
       abcissa(1:5) = (/-0.93400143040806_DP, -0.78448347366314_DP, &
            & -0.56523532699621_DP, -0.29575813558694_DP, 0.0_DP/)
       abcissa(6:9) = -abcissa(4:1:-1)
       weight(1:5) = (/0.10961227326699_DP, 0.18716988178031_DP, &
            & 0.24804810426403_DP, 0.28687912477901_DP, &
            & 0.30021759545569_DP/)
       weight(6:9) = weight(4:1:-1)
    case(12)
       abcissa(1:5) = (/-0.94489927222288_DP, -0.81927932164401_DP, &
            & -0.63287615303186_DP, -0.39953094096535_DP, &
            & -0.13655293285493_DP/)
       abcissa(6:10) = -abcissa(5:1:-1)
       weight(1:5) = (/0.09168451741320_DP, 0.15797470556437_DP, &
            & 0.21250841776102_DP, 0.25127560319920_DP, &
            & 0.27140524091070_DP/)
       weight(6:10) = weight(5:1:-1)
    case default
       print *, 'unsupported Gauss-Lobatto integration order selected'
       stop
    end select
  end subroutine gauss_lobatto_setup


  !! ###################################################
  !! wynn-epsilon acceleration of partial sums, given a series
  function wynn_epsilon(series,debug) result(accsum)
    use constants, only : RONE, RZERO

    implicit none
    integer, parameter :: MINTERMS = 4
    real(DP), dimension(1:), intent(in) :: series
    integer, intent(in) :: debug
    real(DP) :: denom, accsum
    integer :: np, i, j, m
    integer :: mm

    real(DP), dimension(1:size(series),-1:size(series)-1) :: ep

    np = size(series)

    ! first column is intiallized to zero
    ep(:,-1) = RZERO

    ! build up partial sums, but check for problems
    check: do i=1,np
       if((abs(series(i)) > huge(RZERO)).or.(series(i) /= series(i))) then
          ! +/- Infinity or NaN ? truncate series to avoid corrupting accelerated sum
          np = i-1
          if(np < MINTERMS) then
             write(*,'(A)',advance='no') 'not enough Wynn-Epsilon series to accelerate'
             accsum = 1.0/0.0  ! make it clear answer is bogus
             goto 777
          else
             write(*,'(A,I3,A)',advance='no') 'Wynn-Epsilon series&
                  &, truncated to ',np,' terms.'
             exit check
          end if
       else
          ! term is good, continue
          ep(i,0) = sum(series(1:i))
       end if
    end do check

    ! build up epsilon table (each column has one less entry)
    do j=0,np-2
       do m = 1,np-(j+1)
          denom = ep(m+1,j) - ep(m,j)
          if(abs(denom) > tiny(1.0_DP)) then ! check for div by zero
             ep(m,j+1) = ep(m+1,j-1) + RONE/denom
          else
             accsum = ep(m+1,j)
             write(*,'(A,I2,1X,I2,A)',advance='no') 'epsilon cancel ',m,j,' '
             if (debug > 1) then
                write(*,*) 'ep'
                do mm=1,np
                   write(*,'(ES11.3E3,1X)') ep(mm,:)
                end do
                write(*,*) 'accsum'
                write(*,*) accsum
             end if
             goto 777
          end if
       end do
    end do

    ! if made all the way through table use "corner value" of triangle as answer
    if(mod(np,2) == 0) then
       accsum = ep(2,np-2)  ! even number of terms - corner is acclerated value
    else
       accsum = ep(2,np-3)  ! odd numbers, use one column in from corner
    end if
777 continue

  end function wynn_epsilon

  subroutine polint(xa,ya,x,y,dy)
    ! xa and ya are given x and y locations to fit an nth degree polynomial
    ! through.  results is a value y at given location x, with error estimate dy

    use constants, only : DP
    implicit none
    real(DP), dimension(:), intent(in) :: xa,ya
    real(DP), intent(in) :: x
    real(DP), intent(out) :: y,dy
    integer :: m,n,ns
    real(DP), dimension(size(xa)) :: c,d,den,ho
    integer, dimension(1) :: tmp

    n = size(xa)
    c = ya
    d = ya
    ho = xa-x
    tmp = minloc(abs(x-xa))
    ns = tmp(1)
    y = ya(ns)
    ns = ns-1
    do m = 1,n-1
       den(1:n-m) = ho(1:n-m)-ho(1+m:n)
       if (any(den(1:n-m) == 0.0)) then
          print *, 'polint: calculation failure'
          stop
       end if
       den(1:n-m) = (c(2:n-m+1)-d(1:n-m))/den(1:n-m)
       d(1:n-m) = ho(1+m:n)*den(1:n-m)
       c(1:n-m) = ho(1:n-m)*den(1:n-m)
       if (2*ns < n-m) then
          dy = c(ns+1)
       else
          dy = d(ns)
          ns = ns-1
       end if
       y = y+dy
    end do
  end subroutine polint

end program Driver
