0  T  8.5D-1  T           :: debug (0=quiet,1=some,2=lots), dimensionless?, rtank, wynn epsilon acceleration?
8.3333D-6  3060.0 18600.0                          :: pumping rate, pump and recovery time
2.0D-1   4.0D-1   2.0D-1                  :: b1, b2, b3
4.0E-4  1.0  6.0E-4  0.25  1.0       :: aquifer Kr, kappa, Ss, Sy, and beta
2.0E-3  2.0E-3  2.0E+0            :: sigma for layers 1,2,3
1.0E-5  1.0E-5  1.0E-5            :: gamma*ell for layers 1,2,3
%%r%%  %%z%%                  :: obs loc: r, depth
1.0D-7  1.0D-8  30         :: deHoog parameters
60                         :: finite hankel sum length
T  3  5  100  times.dat   :: calc t?, min log_10(t), max log_10(t), numt, t filename
0.00   0.00              :: slope, intercept of trend
fhank-r%%r%%-z%%z%%.out