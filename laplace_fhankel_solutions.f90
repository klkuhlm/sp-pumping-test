!
! Copyright (c) 2011 Kristopher L. Kuhlman (klkuhlm at sandia dot gov)
!
! Permission is hereby granted, free of charge, to any person obtaining a copy
! of this software and associated documentation files (the "Software"), to deal
! in the Software without restriction, including without limitation the rights
! to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
! copies of the Software, and to permit persons to whom the Software is
! furnished to do so, subject to the following conditions:
!
! The above copyright notice and this permission notice shall be included in
! all copies or substantial portions of the Software.
!
! THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
! IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
! FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
! AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
! LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
! OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
! THE SOFTWARE.
!
! Malama, B., A. Revil, and K.L. Kuhlman, 2009. A semi-analytical solution
!  for transient streaming potentials associated with confined aquifer
!  pumping tests, Geophysical Journal International, 176(3), 1007–1016.
! http://dx.doi.org/10.1111/j.1365-246X.2008.04014.x
!
! Malama, B., K.L. Kuhlman, and A. Revil, 2009. Theory of transient
!  streaming potentials associated with axial-symmetric flow in unconfined
!  aquifers, Geophysical Journal International, 179(2), 990–1003.
! http://dx.doi.org/10.1111/j.1365-246X.2009.04336.x
!

module three_layer_finite
  implicit none

  private
  public :: unconfined_aquifer_sp, neuman72

contains

  pure function unconfined_aquifer_sp(a,p,pm,zd,bd1,bd3,layer,sigmaD,kappa,varTheta,betaD) result(fp)
    use constants, only : EP,DP

    implicit none
    real(EP), intent(in), dimension(:) :: a
    complex(EP), intent(in), dimension(:) :: p, pm
    real(DP), intent(in) :: zd,bd1,bd3, kappa, varTheta, betaD
    real(DP), dimension(3), intent(in) :: sigmaD
    logical, dimension(3), intent(in) :: layer
    complex(EP), dimension(size(a),size(p)) :: fp

    integer :: np, nj
    complex(EP), dimension(0:1,size(a),size(p)) :: neuman
    complex(EP), dimension(2,size(a),size(p)) :: g,f
    complex(EP), dimension(2,size(a)) :: AYE
    complex(EP), dimension(size(a),size(p)) :: delta
    complex(EP), dimension(1:2,size(a),size(p)) :: xi
    complex(EP), dimension(1:5,size(a)) :: coshx,sinhx

    np = size(p)
    nj = size(a)

    coshx(1,1:nj) = cosh(a(1:nj)*bd1)
    sinhx(1,1:nj) = sinh(a(1:nj)*bd1)

    coshx(2,1:nj) = cosh(a(1:nj)*bd3)
    sinhx(2,1:nj) = sinh(a(1:nj)*bd3)

    coshx(3,1:nj) = cosh(a(1:nj))
    sinhx(3,1:nj) = sinh(a(1:nj))

    coshx(4,1:nj) = cosh(a(1:nj)*zd)
    sinhx(4,1:nj) = sinh(a(1:nj)*zd)

    coshx(5,1:nj) = cosh(a(1:nj)*(1.0 - zd))
    sinhx(5,1:nj) = sinh(a(1:nj)*(1.0 - zd))

    g(1,1:nj,1:np) = spread(coshx(1,1:nj)*coshx(5,1:nj) + &
         & sigmaD(1)*sinhx(1,1:nj)*sinhx(5,1:nj),2,np)
    g(2,1:nj,1:np) = spread(coshx(2,1:nj)*coshx(4,1:nj) + &
         & sigmaD(3)*sinhx(2,1:nj)*sinhx(4,1:nj),2,np)

    f(1,1:nj,1:np) = spread((coshx(2,1:nj)*coshx(3,1:nj) + &
         & sigmaD(3)*sinhx(2,1:nj)*sinhx(3,1:nj))/coshx(1,1:nj),2,np)
    f(2,1:nj,1:np) = spread(sigmaD(1)*coshx(3,1:nj)*sinhx(1,1:nj) + &
         & sinhx(3,1:nj)*coshx(1,1:nj),2,np)

    AYE(1,1:nj) = coshx(1,1:nj)*coshx(2,1:nj) + &
         & sigmaD(1)*sigmaD(3)*sinhx(1,1:nj)*sinhx(2,1:nj)
    AYE(2,1:nj) = sigmaD(1)*sinhx(1,1:nj)*coshx(2,1:nj) + &
         & sigmaD(3)*coshx(1,1:nj)*sinhx(2,1:nj)

    delta(1:nj,1:np) = spread(abs(AYE(1,1:nj)*sinhx(3,1:nj) + &
         & AYE(2,1:nj)*coshx(3,1:nj)),2,np)

    ! alphaD = kappa/varTheta
    neuman(0,1:nj,1:np) = neuman72(p(1:np),a(1:nj),pm(1:np),0.0_DP,kappa,kappa/varTheta,betaD)
    neuman(1,1:nj,1:np) = neuman72(p(1:np),a(1:nj),pm(1:np),1.0_DP,kappa,kappa/varTheta,betaD)

    xi(1,1:nj,1:np) = sigmaD(3)*neuman(0,1:nj,1:np)*spread(sinhx(2,1:nj),2,np)
    ! 1/alphaD term missing in manuscript
    xi(2,1:nj,1:np) = neuman(1,1:nj,1:np)*(sigmaD(1)*spread(sinhx(1,1:nj),2,np) - &
         & spread(p(1:np),1,nj)*varTheta/kappa*spread(coshx(1,1:nj)/a(1:nj),2,np))

    if(layer(1)) then  ! top unpumped vadose zone
       fp(1:nj,1:np) = ((xi(1,1:nj,1:np) + xi(2,1:nj,1:np)*f(1,1:nj,1:np))/delta(1:nj,1:np) - &
            & neuman(1,1:nj,1:np)/spread(coshx(1,:),2,np))*&
            & spread(cosh(a(1:nj)*(1.0 + bd1 - zd)),2,np)
    elseif(layer(2)) then  ! middle pumped unconfined layer
       fp(1:nj,1:np) = (xi(1,1:nj,1:np)*g(1,1:nj,1:np) + xi(2,1:nj,1:np)*g(2,1:nj,1:np))/delta(1:nj,1:np) - &
            & neuman72(p(:),a(:),pm(:),zd,kappa,kappa/varTheta,betaD)
    else ! lower no-flow aquiclude
       fp(1:nj,1:np) = (xi(2,1:nj,1:np) - neuman(0,1:nj,1:np)*f(2,1:nj,1:np))/delta(1:nj,1:np)* &
            & spread(cosh(a(1:nj)*(bd3 + zd)),2,np)
    end if

  end function unconfined_aquifer_sp

  !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
  !! this function is modified from the leaky-unconfined code
  !! this actually implements Bwalya's alternative linearization
  pure function neuman72(p,a,pm,zd,kappa,alphaD,betaD) result(n72)
    use constants, only : EP,DP
    use complex_hyperbolic, only : ccosh, csinh

    implicit none
    real(DP), intent(in) :: zd,kappa,alphaD,betaD
    real(EP), intent(in), dimension(:) :: a
    complex(EP), intent(in), dimension(:) ::  p, pm
    complex(EP), dimension(size(a),size(p)) :: n72,theis,eta,xi
    integer :: np, nj

    np = size(p)
    nj = size(a)

    eta(1:nj,1:np) = sqrt((spread(p(1:np),1,nj) + spread(a(1:nj)**2,2,np))/kappa)
    xi(1:nj,1:np) = eta(1:nj,1:np)*alphaD/spread(p(1:np),1,nj)
    theis(1:nj,1:np) = 2.0*spread(pm(1:np),1,nj)/ &
         & (spread(p(1:np),1,nj) + spread(a(1:nj)**2,2,np))

    n72(1:nj,1:np) = theis(:,:)*(1.0 - ccosh(eta(:,:)*zd)/ &
         & ((1.0 + betaD*eta(:,:)*xi(:,:))*ccosh(eta(:,:)) + &
         & xi(:,:)*csinh(eta(:,:))))

  end function neuman72

end module three_layer_finite
