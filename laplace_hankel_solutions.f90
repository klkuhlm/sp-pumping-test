!
! Copyright (c) 2008 Kristopher L. Kuhlman (kuhlman at hwr dot arizona dot edu)
!
! Permission is hereby granted, free of charge, to any person obtaining a copy
! of this software and associated documentation files (the "Software"), to deal
! in the Software without restriction, including without limitation the rights
! to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
! copies of the Software, and to permit persons to whom the Software is
! furnished to do so, subject to the following conditions:
!
! The above copyright notice and this permission notice shall be included in
! all copies or substantial portions of the Software.
!
! THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
! IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
! FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
! AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
! LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
! OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
! THE SOFTWARE.
!
! Malama, B., A. Revil, and K.L. Kuhlman, 2009. A semi-analytical solution
!  for transient streaming potentials associated with confined aquifer
!  pumping tests, Geophysical Journal International, 176(3), 1007–1016.
! http://dx.doi.org/10.1111/j.1365-246X.2008.04014.x
!
! Malama, B., K.L. Kuhlman, and A. Revil, 2009. Theory of transient
!  streaming potentials associated with axial-symmetric flow in unconfined
!  aquifers, Geophysical Journal International, 179(2), 990–1003.
! http://dx.doi.org/10.1111/j.1365-246X.2009.04336.x
!

! $Id: laplace_hankel_solutions.f90,v 1.3 2009/03/15 03:38:14 kris Exp kris $

module three_layer
  implicit none

  private
  public :: unconfined_aquifer_sp

contains

  function unconfined_aquifer_sp(a,p,zd,bd1,bd3,layer,sigmaD,kappa,alphaD) result(fp)
    use constants, only : EP,DP

    implicit none
    real(DP), intent(in), dimension(:) :: a
    complex(EP), intent(in), dimension(:) :: p
    real(DP), intent(in) :: zd,rD,bd1,bd3,kappa,alphaD
    complex(EP), dimension(size(a),size(p)) :: fp

    integer :: np,nj
    complex(EP), dimension(0:1,size(a),size(p)) :: neuman
    complex(EP), dimension(2,size(a),size(p)) :: g,f
    complex(EP), dimension(2,size(a)) :: AYE
    complex(EP), dimension(size(a),size(p)):: delta
    complex(EP), dimension(1:2,size(a),size(p)) :: xi
    complex(EP), dimension(1:5,size(a)) :: coshx,sinhx

    np = size(p)
    nj = size(a)

    coshx(1,1:nj) = cosh(a*bd1)
    sinhx(1,1:nj) = sinh(a*bd1)
    coshx(2,1:nj) = cosh(a*bd3)
    sinhx(2,1:nj) = sinh(a*bd3)
    coshx(3,1:nj) = cosh(a)
    sinhx(3,1:nj) = sinh(a)
    coshx(4,1:nj) = cosh(a*zd)
    sinhx(4,1:nj) = sinh(a*zd)
    coshx(5,1:nj) = cosh(a*(1.0 - zd))
    sinhx(5,1:nj) = sinh(a*(1.0 - zd))

    g(1,1:nj,1:np) = spread(coshx(1,:)*coshx(5,:) + &
         & sigmaD(1)*sinhx(1,:)*sinhx(5,:),2,np)
    g(2,1:nj,1:np) = spread(coshx(2,:)*coshx(4,:) + &
         & sigmaD(3)*sinhx(2,:)*sinhx(4,:),2,np)

    f(1,1:nj,1:np) = spread((coshx(2,:)*coshx(3,:) + &
         & sigmaD(3)*sinhx(2,:)*sinhx(3,:))/coshx(1,:),2,np)
    f(2,1:nj,1:np) = spread(sigmaD(1)*coshx(3,:)*sinhx(1,:) + &
         & sinhx(3,:)*coshx(1,:),2,np)

    AYE(1,1:nj) = coshx(1,:)*coshx(2,:) + &
         & sigmaD(1)*sigmaD(3)*sinhx(1,:)*sinhx(2,:)
    AYE(2,1:nj) = sigmaD(1)*sinhx(1,:)*coshx(2,:) + &
         & sigmaD(3)*coshx(1,:)*sinhx(2,:)

    delta(1:nj,1:np) = spread(abs(AYE(1,:)*sinhx(3,:) + &
         & AYE(2,:)*coshx(3,:)),2,np)

    neuman(0,1:nj,1:np) = neuman72(p(:),a(:),0.0,kappa,alphaD)
    neuman(1,1:nj,1:np) = neuman72(p(:),a(:),1.0,kappa,alphaD)

    xi(1,1:nj,1:np) = sigmaD(3)*neuman(0,:,:)*spread(sinhx(2,:),2,np)
    xi(2,1:nj,1:np) = neuman(1,:,:)*(sigmaD(1)*spread(sinhx(1,:),2,np) - &
         & spread(p(:),1,nj)*spread(coshx(1,:)/(a(:)*alphaD),2,np))

    if(layer(1)) then  ! top unpumped vadose zone
       fp(1:nj,1:np) = ((xi(1,:,:) + xi(2,:,:)*f(1,:,:))/delta(:,:) - &
            & neuman(1,:,:)/spread(coshx(1,:),2,np))* &
            & spread(cosh(a(:)*(1.0 + bd1 - zd)),2,np)
    elseif(layer(2)) then  ! middle pumped unconfined layer
       fp(1:nj,1:np) = (xi(1,:,:)*g(1,:,:) + xi(2,:,:)*g(2,:,:))/delta(:,:) - &
            & neuman72(p(:),a(:),zd,kappa,alphaD)
    else ! lower no-flow aquiclude
       fp(1:nj,1:np) = (xi(2,:,:) - neuman(0,:,:)*f(2,:,:))/delta(:,:)* &
            & spread(cosh(a(:)*(bd3 + zd)),2,np)
    end if

  end function unconfined_aquifer_sp

  !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
  !! this function is modified from the leaky-unconfined code
  !! it is only designed to be called from the one_aquifer_sp routine above
  pure function neuman72(p,dum,zd,kappa,alphaD) result(epfp)
    use constants, only : EP,DP,RTWO,ETWO,PIEP,EONE

    implicit none
    real(DP), intent(in) :: zd,kappa,alphaD
    real(EP), intent(in), dimension(:) :: a
    complex(EP), intent(in), dimension(:) :: p
    complex(EP), dimension(size(a),size(p)) :: n72,theis,eta,xi
    integer :: np,nj

    np = size(p)
    nj = size(a)

    eta(1:nj,1:np) = sqrt((spread(p,1,nj) + spread(a**2,2,np))/kappa)
    xi(1:nj,1:np) = eta(:,:)*alphaD/spread(p,1,nj)
    theis(1:nj,1:np) = 2.0/(spread(p,1,nj) + spread(a**2,2,np)) ! less factor 1/p

    n72(1:nj1:np) = theis(:,:)*(1.0 - cosh(eta(:,:)*zd)/ &
         & (cosh(eta(:,:)) + xi(:,:)*sinh(eta(:,:))))

  end function neuman72

end module three_layer
