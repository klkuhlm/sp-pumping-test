import numpy as np
import matplotlib
matplotlib.use('Agg')
import matplotlib.pyplot as plt
from glob import glob

fig = plt.figure(figsize=(18,18))
axrphi = fig.add_subplot(221)
axzphi = fig.add_subplot(222)
axrhead = fig.add_subplot(223)
axzhead = fig.add_subplot(224)

b2 = 0.4

colors = ['red','green','blue','black','cyan','magenta','pink','orange','gray']

for j,fr in enumerate(glob('fhank-r*-z0.5.out')):

    s = fr.lstrip('fhank-').rstrip('.out').split('-')

    rDval = float(s[0].lstrip('r'))/b2
    zDval = float(s[1].lstrip('z'))/b2

    t,phi,dphi,head,dhead = np.loadtxt(fr,skiprows=16,unpack=True,usecols=(0,1,2,4,5))

    axrphi.semilogx(t,phi,'-',color=colors[j]) 
    #axrphi.semilogx(t,dphi,'--',color=colors[j])
    axrphi.set_xlabel('$t_D$')
    axrphi.set_ylabel('$\\phi_D$ and $\\partial \\phi_D/\\partial(\\log(t))$')
    #axrphi.set_ylim((-2,2))

    axrhead.loglog(t,head,'-',color=colors[j],label='$r_D=$%.2f $z_D=$%.2f' % (rDval,zDval)) 
    #axrhead.loglog(t,dhead,'--',color=colors[j])
    axrhead.legend()
    axrhead.set_xlabel('$t_D$')
    axrhead.set_ylabel('$s_D$ and $\\partial s_D/\\partial(\\log(t))$')
    axrhead.set_ylim((1.0E-4,1.0E0))

for j,fz in enumerate(glob('fhank-r0.3-z*.out')):

    s = fz.lstrip('fhank-').rstrip('.out').split('-')

    rDval = float(s[0].lstrip('r'))/b2
    zDval = float(s[1].lstrip('z'))/b2

    t,phi,dphi,head,dhead = np.loadtxt(fz,skiprows=16,unpack=True,usecols=(0,1,2,4,5))

    axzphi.semilogx(t,phi,'-',color=colors[j]) 
    #axzphi.semilogx(t,dphi,'--',color=colors[j])
    axzphi.set_xlabel('$t_D$')
    axzphi.set_ylabel('$\\phi_D$ and $\\partial \\phi_D/\\partial(\\log(t))$')
    #axzphi.set_ylim((-2,2))

    axzhead.loglog(t,head,'-',color=colors[j],label='$r_D=$%.2f $z_D=$%.2f' % (rDval,zDval)) 
    #axzhead.loglog(t,dhead,'--',color=colors[j])
    axzhead.legend()
    axzhead.set_xlabel('$t_D$')
    axzhead.set_ylabel('$s_D$ and $\\partial s_D/\\partial(\\log(t))$')
    axzhead.set_ylim((1.0E-4,1.0E0))
    
plt.savefig('sp-pumping-test-solution.png')
