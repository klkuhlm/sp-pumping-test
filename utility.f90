!
! Copyright (c) 2011 Kristopher L. Kuhlman (klkuhlm at sandia dot gov)
!
! Permission is hereby granted, free of charge, to any person obtaining a copy
! of this software and associated documentation files (the "Software"), to deal
! in the Software without restriction, including without limitation the rights
! to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
! copies of the Software, and to permit persons to whom the Software is
! furnished to do so, subject to the following conditions:
!
! The above copyright notice and this permission notice shall be included in
! all copies or substantial portions of the Software.
!
! THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
! IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
! FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
! AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
! LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
! OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
! THE SOFTWARE.
!
! Malama, B., A. Revil, and K.L. Kuhlman, 2009. A semi-analytical solution
!  for transient streaming potentials associated with confined aquifer
!  pumping tests, Geophysical Journal International, 176(3), 1007–1016.
! http://dx.doi.org/10.1111/j.1365-246X.2008.04014.x
!
! Malama, B., K.L. Kuhlman, and A. Revil, 2009. Theory of transient
!  streaming potentials associated with axial-symmetric flow in unconfined
!  aquifers, Geophysical Journal International, 179(2), 990–1003.
! http://dx.doi.org/10.1111/j.1365-246X.2009.04336.x
!

! $Id: utility.f90,v 1.3 2009/03/15 03:38:31 kris Exp kris $

!! this file has _three_ modules in it

!! ######################################################################
!! ######################################################################

module constants

  ! real with range 300 orders of mag, 15 sig figs (8 on both g95 & ifort)
  integer, parameter :: DP = selected_real_kind (p=15,r=300)

  !! full quad precision
  integer, parameter :: QP = selected_real_kind(p=33,r=3000)

  !! extended range internal variables (10 on g95, 16 on ifort)
  integer, parameter :: EP = selected_real_kind(r=3000)


  ! useful? constants related to pi and ln
  ! calculated to precision=33 using ifort or mathematica
  real(DP), parameter :: PI =        3.141592653589793238462643383279503_DP
  real(DP), parameter :: TWOPI =     6.28318530717958647692528676655901_DP
  real(DP), parameter :: PIOV2 =     1.57079632679489661923132169163975_DP
  real(DP), parameter :: PIOV3 =     1.04719755119659774615421446109317_DP
  real(DP), parameter :: PIOV4 =     0.785398163397448309615660845819876_DP
  real(DP), parameter :: ONEOVPI =   0.318309886183790671537767526745029_DP
  real(DP), parameter :: LN10  =     2.30258509299404568401799145468436_DP
  real(DP), parameter :: LN2 =       0.693147180559945309417232121458177_DP
  real(DP), parameter :: LNPIOV2 =   0.451582705289454864726195229894882_DP
  real(DP), parameter :: EULER =     0.5772156649015328606065120900824025_DP
  real(DP), parameter :: PISQ =      9.86960440108935861883449099987615_DP
  real(DP), parameter :: SQRTPI =    1.77245385090551602729816748334115_DP
  real(DP), parameter :: SQRTPIOV2 = 1.25331413731550025120788264240552_DP
  real(DP), parameter :: INVLOG10 =  0.434294481903251827651128918916605_DP

  real(EP), parameter :: LN2EP = 0.693147180559945309417232121458177_EP
  real(QP), parameter :: LN2QP = 0.693147180559945309417232121458177_QP
  real(EP), parameter :: PIEP = 3.141592653589793238462643383279503_EP


end module constants

!! ######################################################################
!! ######################################################################

module complex_hyperbolic
  private
  public :: ccosh, csinh

  ! these functions are implemented natively by gfortan, but are not included
  ! in intel compiler library.  They are added here for compatibility.

contains
  elemental function ccosh(z)
    use constants, only : EP
  
    complex(EP), intent(in) :: z
    complex(EP) :: ccosh
    real(EP) :: x,y
    x = real(z)
    y = aimag(z)
    
    ccosh = cmplx(cosh(x)*cos(y),sinh(x)*sin(y),EP) 
  end function ccosh

  elemental function csinh(z)
    use constants, only : EP
  
    complex(EP), intent(in) :: z
    complex(EP) :: csinh
    real(EP) :: x,y
    x = real(z)
    y = aimag(z)
    
    csinh = cmplx(sinh(x)*cos(y),cosh(x)*sin(y),EP) 
  end function csinh

end module complex_hyperbolic
